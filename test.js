'use strict';

let knex = require('knex')({client: 'oracle'});

let query = knex('order_orders')
                  .where('cust_account_no', 'account_no')
                  .whereNotIn('order_status', ['CAN', 'SCOM'])
                  .orderBy('order_date', 'desc')
                  .limit(16 * (2 - 1))
                  .offset(16 * 2).toString();