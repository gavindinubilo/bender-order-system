'use strict';
const json2csv = require('json2csv');
const order_fields = ["Order Number","Ship To","Item ID","Item Description","Order Qty","Ship Qty","Short Qty","P.O. Number","Req. Ship Date","Ship Date","Order Status","Pro Tracking Numbers"]
const inventory_fields = ["Item","Description 1","Description 2","On Hand","Available","On Hold","Damaged","On Order","Product Type"];

function build_order_report(orders, cb) {
  orders = orders.map(function(order) {
    order['Order Number'] = order.CUSTOMER_ORDER_NO;
    order['Ship To'] = order.SHIP_TO_ADDRESS;
    order['Item ID'] = order.ITEM_ID;
    order['Item Description'] = order.ITEM_DESCR_1;
    order['Order Qty'] = order.ORD_QTY;
    order['Ship Qty'] = order.SHIP_QTY;
    order['Short Qty'] = order.SHORT_QTY;
    order['P.O. Number'] = order.PO_NUMBER;
    order['Req. Ship Date'] = order.REQ_SHIP_DATE;
    order['Ship Date'] = order.SHIP_DATE;
    order['Order Status'] = order.ORDER_STATUS;    
    order['Pro Tracking Numbers'] = order.PRO_NO;    
    order['Pro Tracking Numbers'] = order.PRO_NO;
    
    return order;
  });
  
  json2csv({ data: orders, fields: order_fields, defaultValue: '-'}, function(err, csv) {
    if (err) console.log(err);
    return cb(csv);
  });
}

function build_inventory_report(inventory, cb) {
  inventory = inventory.map(function(item) {
    item['Item'] = item.ITEM_ID;
    item['On Hand'] = item.ON_HAND_QTY;
    item['Available'] = item.AVAIL_QTY;
    item['On Hold'] = item.ON_HOLD_QTY;
    item['Damaged'] = item.DMGD_QTY;
    item['On Order'] = item.OPEN_ORDS_QTY;
    item['Description 1'] = item.ITEM_DESCR_1;
    item['Description 2'] = item.ITEM_DESCR_2;
    item['Product Type'] = item.PROD_TYPE;
      
    return item;
  });
  
  json2csv({data: inventory, fields: inventory_fields, defaultValue: '-'}, function(err, csv) {
    if (err) console.log(err);
    return cb(csv);
  });
}

module.exports = {
  order: build_order_report,
  inventory: build_inventory_report
}