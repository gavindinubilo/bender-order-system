var Client = require('scp2').Client;
var fs = require('fs');
var rollbar

module.exports = function(xml, account_no) {
  var file_name = `${account_no}_ord_${Date.now()}.xml`;
  fs.writeFile(file_name, xml, function(fs_err) {
    if (process.env['API_ENV'] !== 'production') return;

    var client = new Client({
      port: 22,
      host: process.env['XML_SERVER_HOST'],
      username: process.env['XML_SERVER_USERNAME'],
      password: process.env['XML_SERVER_PASSWORD']
    });

    
    client.upload(file_name, `/home/flexapp/edi/inb/web/${file_name}`, function(scp_err) {
      console.log('uploaded');
      if (scp_err) console.log(scp_err);
      fs.unlink(file_name, (err) => {
        if (err) console.log(err);
      });
    });    
  });  
}