'use strict';
require('./string');
const util = require('util');

// Build better error messages for mongoose
function get_error_from_11000(error) {
  var field = error.message.split('.$')[1];
  field = field.split(' dup key')[0];
  field = field.substring(0, field.lastIndexOf('_'));
  return field;
}

var messages = {
  'required': "%s is required.",
  'min': "%s below minimum.",
  'max': "%s above maximum.",
  'enum': "%s not an allowed value."
};

function convert_error(error) {
  if (error.code === 11000 || error.code === 11001) {
    return (get_error_from_11000(error) + ' already exists').titleize();
  }
  
  error = util.format(messages[error.kind], error.path).titleize();
  
  return  error;
}

module.exports = {
  build: function(errors) {    
    // If error has code, then return that error
    if (errors.code) {
      return convert_error(errors);
    }    

    // Loop through all errors    
    let errs = [];
    for (let error_key in errors.errors) {
      errs.push(convert_error(errors.errors[error_key]));       
    }
    
    return errs; 
  }
}