const goback_btn = document.querySelectorAll('.js--goback');

if (goback_btn.length) {
  goback_btn.forEach((btn) => {
    btn.addEventListener('click', function() {
      window.history.go(-1);
      return false;
    });
  });
}