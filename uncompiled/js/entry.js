'use strict'
var rollbarConfig = {
  accessToken: '4989c9c0fa724480aa3119bc9ca2657c',
  captureUncaught: true,
  payload: {
    environment: 'production',
  }
};

var Rollbar = require('./rollbar.umd.nojson.min.js').init(rollbarConfig);

const React = require('react');
const ReactDom = require('react-dom');
const swal = require('sweetalert');
const _ = require('underscore');

require('./detectIE.js');
require('./polyfill.js');
require('es6-promise');
require('./fetch.js');
require('./goback.js');

try {
  const UserForm = React.createFactory(require('../../views/shared/forms/user.jsx'));
  const OrderForm = React.createFactory(require('../../views/shared/forms/order.jsx'));
  const InventoryList = React.createFactory(require('../../views/shared/listings/inventory.jsx'));
  const NotificationList = React.createFactory(require('../../views/shared/listings/notification.jsx'));
  const OrderSearch = React.createFactory(require('../../views/shared/forms/search.jsx'));
  const AddressForm = React.createFactory(require('../../views/shared/forms/address.jsx'));
  const current_user = JSON.parse(window.localStorage.getItem('current_user'));

  if (document.getElementById('react--user-form')) {
    const user_data = JSON.parse(window.localStorage.getItem('user-form-data')) || {};
    const user_props = _.extend(user_data, {current_user: current_user});
    ReactDom.render(new UserForm(user_props), document.getElementById('react--user-form'));
  }

  if (document.getElementById('react--order-form')) {
    const order_data = JSON.parse(decodeURI(window.localStorage.getItem('order-form-data'))) || {};
    const order_props = _.extend(order_data, {current_user: current_user});
    ReactDom.render(new OrderForm(order_props), document.getElementById('react--order-form'));
  }

  if (document.getElementById('react--inventory-list')) {
    const inventory_data = JSON.parse(decodeURI(window.localStorage.getItem('inventory-data'))) || {};
    const inventory_props = _.extend(inventory_data, {current_user: current_user});
    ReactDom.render(new InventoryList(inventory_props), document.getElementById('react--inventory-list'));
  }

  if (document.getElementById('react--notification-list')) {
    const notification_data = JSON.parse(decodeURI(window.localStorage.getItem('notification-data'))) || {};
    const notification_props = _.extend(notification_data, {current_user: current_user});
    ReactDom.render(new NotificationList(notification_props), document.getElementById('react--notification-list'));
  }

  if (document.getElementById('react--order-search')) {
    const search_data = JSON.parse(window.localStorage.getItem('search-data'));
    ReactDom.render(new OrderSearch(search_data), document.getElementById('react--order-search'));
  }

  if (document.getElementById('react--address-form')) {
    const address_data = JSON.parse(decodeURI(window.localStorage.getItem('address-data'))) || {};
    ReactDom.render(new AddressForm(address_data), document.getElementById('react--address-form'));
  }

  require('./search-pagination');

  window.localStorage.clear();
} catch (e) {
  Rollbar.error('Browser error', e);
}