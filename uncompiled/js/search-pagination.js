if (window.location.pathname === '/orders/search') {
  var prev_wrap = document.querySelector('.prev-wrapper a');
  var next_wrap = document.querySelector('.next-wrapper a');
  var url = window.location.search.replace(/\&page=[0-9]*/, '');
  if (prev_wrap) {
    prev_wrap.href = url + '&page=' + prev_wrap.dataset.page;
  }
  if (next_wrap) {
    next_wrap.href = url + '&page=' + next_wrap.dataset.page;
  }
}