'use strict';

module.exports = function(mongoose) {
  const Schema = mongoose.Schema;
  const AddressSchema = new Schema({
    account_no: {
      type: 'Number',
      required: true
    },
    name: {
      type: 'String',
      required: true 
    },
    company_name: {
      type: 'String'  
    },
    line_one: {
      type: 'String',
      required: true 
    },
    line_two: {
      type: 'String' 
    },
    line_three: {
      type: 'String' 
    },
    city: {
      type: 'String',
      required: true 
    },
    state: {
      type: 'String'
    },
    country: {
      type: 'String',
      required: true 
    },
    zip_code: {
      type: 'String',
      required: true 
    },
    email: {
      type: 'String' 
    },
    phone: {
      type: 'String'
    },
    fax: {
      type: 'String' 
    },    
    created_at: {type: Date, default: new Date()},
  });

  const Address = mongoose.model('Address', AddressSchema);

  return Address;
}