'use strict';
let knex = require('knex')({client: 'oracle'});

module.exports = function(Base) {
  function Inventory () {};
  
  Inventory.getItemsForOrder = function(order_no, cb) {
    let query = knex.table('WMS.V_ORDER_DETAIL')
                .where({
                  'ORDER_NO': order_no
                }).toString();
                  
    return Base.run(query, cb);
  }  
  
  Inventory.getProductsForUser = function(account_no, cb) {
    let query = knex.table('V_PRODUCTS').where('ACCOUNT_NO', account_no.toString()).toString();

    return Base.run(query, cb);
  }
  
  Inventory.getProductsForUserByIds = function(account_no, item_ids, cb) {
    let query = knex.table('V_PRODUCTS')
                    .where('ACCOUNT_NO', account_no.toString())
                    .whereRaw('ITEM_ID = ANY(?)', item_ids)
                    .toString();
    
    return Base.run(query, cb);
  }

  Inventory.getProduct = function(account_no, item_id, cb) {
    let query = knex.table('V_PRODUCTS')
                    .where({
                      'ACCOUNT_NO': account_no.toString(),
                      'ITEM_ID': item_id 
                    }).limit(1).toString();
                    
    return Base.run(query, cb);
  }
   
  
  return Inventory; 
}