'use strict';
const knex = require('knex')({client: 'oracle'});
const _ = require('underscore');
const moment = require('moment');

module.exports = function(Base) {
  function Order() {};
  
  Order.getOrdersForUser = function(account_no, page_offset, cb) {
    let query = knex.table('WMS.V_ORDER_HEADER')
                    .where({
                      'ACCOUNT_NO': account_no,
                    })
                    .whereIn('ORDER_TYPE', ['REGULAR', 'DEFERRED'])
                    .whereNotIn('ORDER_STATUS', ['CAN', 'SCOM'])
                    .orderBy('ORDER_DATE', 'desc')
                    .limit(16)
                    .offset(16 * (page_offset- 1)).toString();
    
    return Base.run(query, cb);
  }
  
  Order.getShippedOrdersForUser = function(account_no, page_offset, cb) {
    let query = knex.table('WMS.V_ORDER_HEADER')
                    .where({
                      'ACCOUNT_NO': `${account_no}`,
                      'ORDER_STATUS': 'SCOM',
                      'ORDER_TYPE': 'REGULAR'
                    })
                    .orderBy('ORDER_DATE', 'DESC')
                    .limit(16)
                    .offset(16 * page_offset).toString();
                    
    return Base.run(query, cb);
  }
  
  Order.getOrderForUser = function(account_no, order_no, cb) {
    let query = knex.table('WMS.V_ORDER_HEADER')
                    .where({
                      'ACCOUNT_NO': `${account_no}`,
                      'ORDER_NO': order_no,
                    }).toString();
                                        
    return Base.run(query, cb);
  }
  
  Order.getItemsForOrder = function(order_no, cb) {
    let query = knex.table('WMS.V_ORDER_DETAIL')
                    .where({
                      'ORDER_NO': order_no
                    }).toString();
                    
    return Base.run(query, cb);
  }
  
  Order.getItemsAndOpenOrders = function(account_no, cb) {
    let query = knex.table('WMS.V_ORDER_HEADER')
                    .where({
                      'WMS.V_ORDER_HEADER.ACCOUNT_NO': account_no,
                    })
                    .whereIn('ORDER_TYPE', ['REGULAR', 'DEFERRED'])
                    .whereNotIn('ORDER_STATUS', ['CAN', 'SCOM'])
                    .innerJoin('WMS.V_ORDER_DETAIL', 'WMS.V_ORDER_HEADER.ORDER_NO', 'WMS.V_ORDER_DETAIL.ORDER_NO').toString();
    
    return Base.run(query, cb);
  }
  
  Order.getItemsAndShippedOrders = function(account_no, cb) {
    let query = knex.table('WMS.V_ORDER_HEADER')
                    .where({
                      'WMS.V_ORDER_HEADER.ACCOUNT_NO': `${account_no}`,
                      'ORDER_STATUS': 'SCOM',
                      'ORDER_TYPE': 'REGULAR',
                    })
                    .where('SHIP_DATE', '>', moment().subtract(1, 'week').format('DD-MMM-YY').toUpperCase())                    
                    .innerJoin('WMS.V_ORDER_DETAIL', 'WMS.V_ORDER_HEADER.ORDER_NO', 'WMS.V_ORDER_DETAIL.ORDER_NO').toString();
    
    return Base.run(query, cb);
  }
  
  Order.is_unique_order_number = function(order_num, cust_num, cb) {
    let query = knex.table('WMS.V_ORDER_HEADER')
                    .count('*')
                    .where({
                      'ACCOUNT_NO': cust_num,
                      'CUSTOMER_ORDER_NO': order_num
                    })
                    .whereNot('ORDER_STATUS', 'CAN').toString();
                    
    return Base.run(query, cb);
  }
  
  Order.searchOrdersForUser = function(account_no, options, page_offset, cb) {
    options = _.pick(options, 'bol_no', 'scac', 'po_number', 'pro_no', 'ship_to_address', 'order_date_start', 'order_date_end', 'ship_date_start', 'ship_date_end', 'order_no');
    let query = knex.table('WMS.V_ORDER_HEADER')
                    .where({
                      'ACCOUNT_NO': account_no,
                    })
                    .whereNot('ORDER_STATUS', 'CAN')
                    .orderBy('ORDER_DATE', 'DESC')
                    .limit(16)
                    .offset(16 * page_offset);
                    
    if (options.order_no) query.whereRaw('lower(CUSTOMER_ORDER_NO) like lower(?)', `%${options.order_no}%`);                    
    if (options.bol_no) query.whereRaw('lower(WMS.V_ORDER_HEADER.BOL_NO) like lower(?)', `%${options.bol_no}%`);
    if (options.scac) query.where('SCAC_CODE', options.scac);
    if (options.po_number) query.whereRaw('lower(PO_NUMBER) like lower(?)', `%${options.po_number}%`);
    if (options.pro_no) query.whereRaw('lower(PRO_NO) like lower(?)', `%${options.pro_no}%`);
    if (options.ship_to_address) query.whereRaw('lower(SHIP_TO_ADDRESS) like lower(?)', `%${options.ship_to_address}%`);
    if (options.order_date_start) query.where('ORDER_DATE', '>', moment(options.order_date_start).format('DD-MMM-YY').toUpperCase());
    if (options.order_date_end) query.where('ORDER_DATE', '<', moment(options.order_date_end).format('DD-MMM-YY').toUpperCase());
    if (options.ship_date_start) query.where('SHIP_DATE', '>', moment(options.ship_date_start).format('DD-MMM-YY').toUpperCase());
    if (options.ship_date_end) query.where('SHIP_DATE', '<', moment(options.ship_date_end).format('DD-MMM-YY').toUpperCase());
    query = query.toString(); 
       
    return Base.run(query, cb);
  }
  
  return Order;
}