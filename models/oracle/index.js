'use strict';

// This is to help build the Oracle queries,
// because they're really weird to do by hand might
// as well let an external library handle that
// Docs: knexjs.org
let knex = require('knex')({client: 'oracle'});
let request = require('request');

// Setup the driver for Oracle
//  Oracle driver schema defined in /etc/odbc.ini
if (process.env['API_ENV'] === 'production') {
  var ODBC = require('odbc')();
  var cn = 'DRIVER={Oracle};UID=dev;PASSWORD=dev;';

  ODBC.openSync(cn);
}


// Base function for running Oracle queries,
// ideally we would never use this and should rely on
// the models for getting data from the Oracle
// database. If need be you can build your own query 
// and run it using `Base.run(query)`. 
function Base() {};

Base.run = function(query, cb) {
  if (process.env['API_ENV'] !== 'production') {
    console.log(`Ran query: ${query}`); 
    return request({method: 'POST', url: 'http://apibeta.bendergroup.com/api/query', json: {query: encodeURI(query)}}, (err, res, body) => {
      cb(err, body);
    });
  }
  var result = ODBC.query(query, (err, data) => cb(err, data));
  return result;
}

module.exports = {
  Base: Base,
  Order: require('./Order.js')(Base),
  Inventory: require('./Inventory.js')(Base),
  Carrier: require('./Carrier.js')(Base),
  Lot: require('./Lot.js')(Base)
}
