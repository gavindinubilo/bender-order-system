// SCAC Code model, mainly only need to get 
// SCAC Codes for dropdown fields

'use strict';
const knex = require('knex')({client: 'oracle'});

module.exports = function(Base) {
  function Carrier() {};
  
  Carrier.getCodes = function(cb) {
    let query = knex.table('WMS.V_WEB_CARRIERS')
                    .select('SCAC_CODE', 'NAME')
                    .orderBy('NAME', 'ASC')
                    .toString();
                    
    return Base.run(query, cb);
  };
  
  return Carrier;
};