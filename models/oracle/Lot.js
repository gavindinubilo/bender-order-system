'use strict';
let knex = require('knex')({client: 'oracle'});

module.exports = function(Base) {
  function Lot () {};

  Lot.getLotsForItem = function(account_no, item_id, cb) {
    let query = knex.table('WMS.V_LOTS')
                    .where({
                      'ACCOUNT_NO': account_no,
                      'ITEM_ID': item_id
                    }).toString();
    Base.run(query, cb);
  }  

  return Lot;
};