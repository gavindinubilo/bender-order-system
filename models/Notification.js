'use strict';

module.exports = function(mongoose) {
  const Schema = mongoose.Schema;
  const NotificationSchema = new Schema({
    account_no: {
      type: 'Number',
      required: true
    },
    type: {
      type: 'String',
      required: true
    },
    text: {
      type: 'String',
      required: true
    },
    link: {
      type: 'String'
    },
    seen: {
      type: 'Boolean',
      default: false,
    },
    created_at: {type: Date, default: new Date()},
  });

  const Notification = mongoose.model('Notification', NotificationSchema);

  return Notification;
}