'use strict';
const WelcomeEmail = require('../mailer/welcome_mailer');
const PasswordResetEmail = require('../mailer/password_reset_mailer');

module.exports = function(mongoose) {
  var Schema = mongoose.Schema;
  var UserSchema = new Schema({
    email: {
      type: 'String',
      unique: true,
      required: true
    },
    username: {
      type: 'String',
      unique: true,
      required: true
    },
    name: {
      type: 'String',
      required: true
    },
    account_no: {
      type: 'Number'
    },
    bender_admin: {
      type: Boolean,
      default: false
    },
    customer_admin: {
      type: Boolean,
      default: false
    },
    need_new_pw: {
      type: Boolean,
      default: true
    },
    created_at: {type: Date, default: new Date()},
    can_add_web_orders: {type: Boolean, default: false},
    can_views_invoices: {type: Boolean, default: false},
    login_key: {type: String, default: Math.random().toString(36).substr(2, 28)},
    api_key: {type: String, default: Math.random().toString(36).substr(2, 28)},
    can_login_from_key: {type: Boolean, default: true}
  });

  UserSchema.methods.setNeedNewPW = function(need_new_pw) {
    this.need_new_pw = need_new_pw;
    this.save(() => {});
  }

  UserSchema.methods.updatePassword = function(pw, cb) {
    this.password = pw;

    this.save(function(err, user) {
      if (err) return cb(err, null);
      this.setNeedNewPW(false);
      cb(null, user);
    }.bind(this));
  }

  UserSchema.methods.setCanLoginFromKey = function(can_login_from_key) {
    this.can_login_from_key = can_login_from_key;
  }

  UserSchema.methods.genLoginKey = function() {
    let login_key = Math.random().toString(36).substr(2, 28);
    this.login_key = login_key;
    this.setCanLoginFromKey(true);
  }

  UserSchema.methods.genTempPW = function() {
    let temp_pw = Math.random().toString(36).substr(2, 28);
    this.password = temp_pw;
    this.save((err) => {});
  }
  
  UserSchema.methods.genApiKey = function() {
    let api_key = Math.random().toString(36).substr(2, 28);
    this.api_key = api_key;
    this.save((err) => {});
  }

  UserSchema.methods.sendPasswordResetEmail = function() {
    const m = new PasswordResetEmail();
    m.send(this);
  }

  UserSchema.plugin(require('basic-auth-mongoose'));
  UserSchema.plugin(require('mongoose-findorcreate'));

  var User = mongoose.model('User', UserSchema);

  return User;
}
