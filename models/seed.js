// This file is to create all the must have records in the database
//  only run this file to setup db
//  can run with `node models/seed.js` from root project directory

require('dotenv').config();
const m = require('./index');
const better_error = require('../services/build_error').build;

// const first_user = new m.User({
//   // email: 'admin@benderwhs.com', 
//   // username : 'admin',
// 	password : 'secret',
//   name: 'Gavin Dinubilo',
//   account_no: 5012,
//   bender_admin: true,
//   customer_admin: true
// });

// first_user.save(function(err, user) {
//   if (err) return console.log(better_error(err));
//   console.log('User is saved and password is encrypted!!');
// });

m.User.find({}).exec(function(err, users) {
  console.log(err);
  for (var user of users) {
    user.genApiKey();
  }
});

for (var i = 0; i < 55; i++) {
  var type = (Math.random() > 0.5) ? 'error' : 'notice';
  var not = new m.Notification({
    account_no: 5012,
    type: type,
    text: Math.random().toString(36).substr(2, 28),
    link: '/',
  });
  not.save((err) => {
    console.log(err);
  });
}