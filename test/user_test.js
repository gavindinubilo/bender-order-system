var assert = require('assert');
var expect = require('chai').expect;

require('dotenv').config();
var User = require('../models').User;

describe('User Models', function() {
  var user = null;

  beforeEach(function(done){
    user = new User({
      email: 'testing@benderwhs.com',
      username : 'test',
      password : 'secret',
      name: 'Test User',
      account_no: 5012,
      bender_admin: true,
      customer_admin: true
    });
    done();
  });

  afterEach(function(done) {
    User.findOneAndRemove({username: 'test'})
    user = null;
    done();
  });

  function save_fails(done) {
    user.save(function(err, createdUser) {
      expect(err).to.exist;
      done();
    });
  }

  it('should validate email present', function(done) {
    user.email = null;
    save_fails(done);
  });

  it('should validate username present', function(done) {
    user.username = null;
    save_fails(done);
  });

  it('should validate name present', function(done) {
    user.name = null;
    save_fails(done);
  });
});
