var assert = require('assert');
var expect = require('chai').expect;

var order_validator = require('../validators/Order_Validator.js');
var test_data = require('./order_validator.json');

describe('Order Validator', function(){
  
  var wrong_data = null;
  
  beforeEach(function(done){    
    wrong_data = JSON.parse(JSON.stringify(test_data));
    done();
  }); 
  
  afterEach(function(done) {
    wrong_data = null;
    done();
  });
  
  it('should require order_number', function() {
    wrong_data.order_number = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
  });
  
  it('should require requested_ship_date', function() {
    wrong_data.requested_ship_date = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
  });
    
  it('should validate requested_ship_date', function() {
    wrong_data.requested_ship_date = '12-1234-12'; // Not formatted correctly
    expect(order_validator.validate(wrong_data)).to.equal(false);
  });
  
  it('should validate request_arrival_date', function() {
    wrong_data.requested_arrival_date = '12-1234-12'; // Not formatted correctly
    expect(order_validator.validate(wrong_data)).to.equal(false);
  });
  
  it('should validate payment_method is in PP, CO or TP', function() {
    wrong_data.payment_method = 'CD';
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.payment_method = 'PP';
    expect(order_validator.validate(wrong_data)).to.equal(true);
    wrong_data.payment_method = 'CO';
    expect(order_validator.validate(wrong_data)).to.equal(true);
    wrong_data.payment_method = 'TP';
    expect(order_validator.validate(wrong_data)).to.equal(true);    
  });
  
  it('should validate notes is less than 500 chars', function() {
    var note_str = '';
    for (var i = 0; i < 510; i++) {
      note_str += 'a';
    }
    wrong_data.notes = note_str;
    expect(order_validator.validate(wrong_data)).to.equal(false);
  });
  
  it('should validate line_item_objects bender_item_id', function() {
    wrong_data.line_items[0].bender_item_id = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
  });
  
  it('should validate line_item_objects quantity', function() {
    wrong_data.line_items[0].quantity = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.line_items[0].quantity = 0;
    expect(order_validator.validate(wrong_data)).to.equal(false);
  });
  
  it('should validate address_type', function() {
    wrong_data.addresses[0].address_type = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].address_type = 'NOT ALLOWED';
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].address_type = 'ST';
    expect(order_validator.validate(wrong_data)).to.equal(true);
    wrong_data.addresses[0].address_type = 'BT';
    expect(order_validator.validate(wrong_data)).to.equal(true);
    wrong_data.addresses[0].address_type = 'FT';
    expect(order_validator.validate(wrong_data)).to.equal(true);        
  });
  
  it('should validate name in address', function() {
    wrong_data.addresses[0].name = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].name = 'Gavin';
    expect(order_validator.validate(wrong_data)).to.equal(true); 
  });
  
  it('should require line_one in address', function() {
    wrong_data.addresses[0].line_one = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].line_one = 'This is a line';
    expect(order_validator.validate(wrong_data)).to.equal(true);
  });
  
  it('should require city in address', function() {
    wrong_data.addresses[0].city = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].city = 'This is a line';
    expect(order_validator.validate(wrong_data)).to.equal(true);
  });
  
  it('should require state in address', function() {
    wrong_data.addresses[0].state = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].state = 'NV';
    expect(order_validator.validate(wrong_data)).to.equal(true);
  });
  
  it('should require zip_code in address', function() {
    wrong_data.addresses[0].zip_code = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].zip_code = '123456';
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].zip_code = '89436';
    expect(order_validator.validate(wrong_data)).to.equal(true);
  });
  
  it('should require country in address', function() {
    wrong_data.addresses[0].country = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].country = 'USA';
    expect(order_validator.validate(wrong_data)).to.equal(true);
  });
  
  it('should validate email is email', function() {
    wrong_data.addresses[0].email = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].email = 'notanemailatbenderwhs.com';
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].email = 'gavind@benderwhs.com';
    expect(order_validator.validate(wrong_data)).to.equal(true);
  });
  
  it('should validate phone is phone', function() {
    wrong_data.addresses[0].phone = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].phone = '123456789-123';
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].phone = '7753762948';
    expect(order_validator.validate(wrong_data)).to.equal(true);
  });
  
  it('should validate fax is fax', function() {
    wrong_data.addresses[0].fax = null;
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].fax = '123456789-123';
    expect(order_validator.validate(wrong_data)).to.equal(false);
    wrong_data.addresses[0].fax = '7753762948';
    expect(order_validator.validate(wrong_data)).to.equal(true);
  });  
});
