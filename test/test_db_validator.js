var assert = require('assert');
var expect = require('chai').expect;

var db_validator = require('../validators/Db_Validator.js');
var test_data = require('./order_validator.json');

describe('DB Validator', function(){
  
  var wrong_data = null;
  
  beforeEach(function(done){    
    wrong_data = JSON.parse(JSON.stringify(test_data));
    done();
  }); 
  
  afterEach(function(done) {
    wrong_data = null;
    done();
  });
  
  it('should validate cust_number', function() {
    var cust_num = '0000'; // Active Account
    expect(db_validator.validate_cust_number(cust_num)).to.equal(true);
    
    cust_num = '5056'; // Inactive Account
    expect(db_validator.validate_cust_number(cust_num)).to.equal(false);
  });  
  
  it('should validate non-duplicate cust_number/customer_order_no', function() {
    var cust_num = '5056';
    var order_num = '052642';
    expect(db_validator.validate_order_number(order_num, cust_num)).to.equal(false);
    
    order_num = '12345789';
    expect(db_validator.validate_order_number(order_num, cust_num)).to.equal(true);
  });
  
  it('should validate invalid item_id', function() {
    var cust_num = '5056';
    var item_id = 'C0250275HC2Z';
    expect(db_validator.validate_bender_item_id(item_id, cust_num)).to.equal(true);
    
    item_id = 'WRONGIDPLEASE';
    expect(db_validator.validate_bender_item_id(item_id, cust_num)).to.equal(false);
  });
});