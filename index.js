// We're using the hapi.js server framework
const Hapi = require('hapi');

// Always start server on 127.0.0.1, default port to 3000 unless env set
const server = new Hapi.Server({ debug: { request: ['error'] } });
server.connection({ host: '0.0.0.0', port: process.env.PORT || 3000 });

require('dotenv').config();

// Required to setup views
//  js and css get written in uncompiled/{respective_folder} first, then run `gulp` to compile into public
//  views are in the views/ folder, all view folders should have a corresponding controller, except for
//  special folders like shared.

require('babel-core/register')({
  plugins: ['transform-react-jsx']
});

server.register(require('inert'), () => { });
server.register(require('vision'), (err) => {
  server.views({
    engines: {
      jsx: require('hapi-react-views')
    },
    compileOptions: {}, // optional
    relativeTo: __dirname,
    path: 'views'
  });
});

server.register({
  register: require('rollbar-hapi'),
  options: {
    accessToken: '99411028bf5841daa9152b769df90389',
    environment: process.env.API_ENV || 'development', // optional, defaults to process.env.NODE_ENV
    exitOnUncaughtException: false // optional, defaults to true
  }
}, function (err) {
  if (err) throw err;
});

server.register({
  register: require('good'),
  options: {
    reporters: [{
      reporter: require('good-console'),
      events: {
        response: '*',
        log: '*'
      }
    }]
  }
}, function (err) { });

// Authentication before requests go through
const auth = require('./auth')(server);

// Setup flash rendering to the frontend
const flashes = require('./flashes')(server);

// For flash messages, accessible through req.session.flash('type', 'message')
var options = {
  storeBlank: false,
  cookieOptions: {
    password: 'password',
    isSecure: false
  }
};

server.register({
  register: require('yar'),
  options: options
}, function (err) { });

// Configure routes
const routes = require('./routes')(server);

// Configure error handlers
const errors = require('./errors')(server);

// Start the server
server.start(() => {
  console.log('Server running at:', server.info.uri);
});
