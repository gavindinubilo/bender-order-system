'use strict';
const Mailer = require('./mailer');

class PasswordResetMailer extends Mailer {
  constructor(props) {
    super();
    this.template = 'password_reset';
  }
  send(user) {
    super.send(this.template, {user: user}, 'Password Reset - Bender Group', user.email);
  }
}

module.exports = PasswordResetMailer;
