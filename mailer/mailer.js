'use strict';
const path           = require('path')
const templatesDir   = path.resolve(__dirname, '..', 'views', 'mail')
const emailTemplates = require('email-templates')
const nodemailer     = require('nodemailer');
const sendgrid       = require('sendgrid')('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'); 

class Mailer {
  send(templateName, props, subject, to) {
    subject = subject || 'Testing';
    to = to || 'gavind@benderwhs.com';

    emailTemplates(templatesDir, function(err, template) {
      template(templateName, props, function(err, html, text) {
        if (err) return console.log(err);

        sendgrid.send({
          from: 'contact@benderwhs.com',
          to: to,
          subject: subject,
          html: html,
          text: text
        }, function(err, json) {
        });
      });
    });
  }
}

module.exports = Mailer;