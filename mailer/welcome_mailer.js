'use strict';
const Mailer = require('./mailer');

class WelcomeMailer extends Mailer {
  constructor(props) {
    super();
    this.props = props;
    this.template = 'welcome';
  }
  send(user) {
    super.send(this.template, {user: user}, 'Welcome - Bender Group', user.email);
  }
}

module.exports = WelcomeMailer;
