# Bender Group
Bender Group ordering system

---

### Intro

Start server with: `npm run start`, this will run both the webserver and the webpack dev server needed for compiling frontend JS.

The backend code for the site is written in node.js, we use React.js for the views, and implement what's called isomorphic JS for rendering forms (they need to be interactive).

Frontend Styling uses a pre-processor called LESS. To compile the LESS into CSS for production run `npm run less` or to continuously compile run `npm run less-watch`.

Frontend JS packages are maintained through NPM (npmjs.com) and compiled using webpack (webpack.github.io). Install packages with `npm install {package name here}`. All packages that you'll need to install should be found on npm, so we can include them in the backend view as well. To build the JS code for production run `npm run js`, to get the JS code in development run `npm run webpack`, this will start a server on your system that serves a compiled version of the JS in `/uncompiled/js`.

For both LESS and JS code, please write them in their respective folder under `/uncompiled`.

We're using MongoDB (https://www.mongodb.com/) for the server database (currently user account stuff), and ODBC for connecting to the Oracle database to pull information about Accounts, and Orders. You will need mongodb running when you develop. 

Hapi.js is the server framework that we are using (hapijs.com). Hapi doesn't have many packages pre-built into it anymore, so we are including those now. Yar is the most important of the ones that we have included it handles all the session stuff for us to keep track of users on the site.

All react.js views are in the `/views` directory. They are organized in the same manner as the `/controllers` directory (more on this later). For every controller there needs to be a view folder, and for each controller method there needs to be a view file inside the respective view folder, except for special use cases where it returns JSON or returns a view that is already present.

### Development

The site is run with node.js, this language doesn't particularly play nice with Windows, so I've been using Vagrant and VirtualBox with Ubuntu Linux to develop, you can use Windows to develop on it. The node version required is `4.2.2`. You can install it from [www.nodejs.org](nodejs.org). To get up and running you need to go into `package.json` and remove the line that says `"odbc": "^1.2.1",` this is required for the production server but won't install locally because of Windows not being compatible. After commenting that out run `npm install` if all goes well you'll get a success message. After that put the line back in otherwise the production site will break.

### Oracle DB

There's really only one file for all the Oracle queries, they're in `models/Order.js` (could possibly be renamed to match what it really is, but for the time being this works). Queries will only work in production and staging, but I've only ever ran the app in production to test Oracle stuff. We're using a node library called `knex` to build the queries (mainly because I've never used Oracle and the query system for it is different than SQL languages I've used). There are plenty of examples of using this in `models/Order.js`. It's fairly straight forward and keeps things organized (less chance of me breaking something with a rogue query).

ODBC connection information on the Digital Ocean server is located in `/etc/odbc.ini` and the server information is set in `.env` in the codebase.

## Rollbar

[Rollbar](http://www.rollbar.com) is the site that contains the errors that happen on the live site, this should be the first place you look when trying to find errors that users are running into. To access ask Dave for login credentials, would probably be beneficial to create a single company wide rollbar account to view the logs.

## NGINX

NGINX configuration is in /etc/nginx/sites-enabled/api when site goes live will need to change everywhere that it says `apibeta` or `customertools-beta` to `customertools`. 

After you make changes remember to run `sudo nginx restart`

## Deploy

### Push To Gitlab

* `git add . -A`
* `git commit -m "informative message here that explains what changed since last commit"`
* `git push origin master`

### Heroku

* Download Heroku toolbelt
* Sign-In with Heroku login, probably get this from Dave
* Push to gitlab (just like above)
* run `git push heroku master`

#### Server Commands

`update-customer-ui` Updates just the frontend, does not restart the server

`update-customer` Updates and restarts the server

`restart-customer` Restarts the server, does not pull updates in

`customer-logs` Prints out logs for server. Though if you're looking for errors the best place

The server currently uses a package called `forever` to keep the site running even after we log out of SSH on the Digital Ocean server. The steps for updating the site go as follows:

SSH into the server, I'm not putting the commands here for security sake. 

`nvm use 4.2.2` This will change the current version of node. Need this because bendergroup.com uses a different version of node than the customer portal.

`cd bender-api` This is where the customer portal is located.

`git pull origin master` Only run this if you have updates to the site, if you don't have updates you can skip this step.

`export API_ENV=production` Need to set the application state to production so the Oracle queries will actually get ran.

`forever restart index.js` This is the proper way to restart the server, I'm looking into a package called `pm2` because it looks more mature than forever. I'll update this readme if I go through that change.

`forever logs index.js` This will show the location of the logfile that `forever` is using, this is the proper way to debug errors, I'm looking into a couple resources that will automate sending errors to us, I haven't found a free version of anything yet.