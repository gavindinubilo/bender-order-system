'use strict';

// Require the controllers
const Joi = require('joi');
const c = require('./controllers/index');
const Gulp = require('./gulpfile.js');

module.exports = function(server) {

  // Base routes
  server.route({method: 'GET', path: '/', handler: c.Base.index});
  server.route({method: 'POST', path: '/', handler: c.Base.api_index});

  // Session routes
  server.route({method: 'GET', path: '/login', handler: c.Session.index});
  server.route({method: 'POST', path: '/login/auth', handler: c.Session.auth});
  server.route({method: 'GET', path: '/logout', handler: c.Session.destroy});

  // User Routes
  server.route({method: 'GET', path: '/update-password', handler: c.Users.change_password});

  let update_password_config = {
    validate: {
      payload: {
        password: Joi.string().required()
      }
    }
  }
  server.route({method: 'POST', path: '/update-password', handler: c.Users.update_password, config: update_password_config});

  let verify_config = {
    validate: {
      query: {
        key: Joi.string().required(),
        user: Joi.string().required()
      }
    }
  }
  server.route({method: 'get', path: '/verify', handler: c.Users.verify, config: verify_config});
  server.route({method: 'get', path: '/reset-password/{username}', handler: c.Users.reset_password});

  // Bender Admin routes
  server.route({method: 'GET', path: '/admin', handler: c.BenderAdmin.index});

    // Bender Admin User Routes
  server.route({method: 'GET', path: '/admin/users', handler: c.BenderAdminUsers.index});
  server.route({method: 'GET', path: '/admin/users/new', handler: c.BenderAdminUsers.new});

  // Permitted params for user creation/updation
  let bender_admin_user_update_config = {
    validate: {
      payload: {
        email: Joi.string().email(),
        name: Joi.string(),
        username: Joi.string(),
        account_no: Joi.string(),
        bender_admin: Joi.boolean(),
        customer_admin: Joi.boolean()
      }
    }
  };
  server.route({method: 'POST', path: '/admin/users/new', handler: c.BenderAdminUsers.create, config: bender_admin_user_update_config});
  server.route({method: 'GET', path: '/admin/users/{username}', handler: c.BenderAdminUsers.show});
  server.route({method: 'GET', path: '/admin/users/{username}/edit', handler: c.BenderAdminUsers.edit});
  server.route({method: 'POST', path: '/admin/users/{username}/update', handler: c.BenderAdminUsers.update, config: bender_admin_user_update_config});
  server.route({method: 'GET', path: '/admin/users/{username}/delete', handler: c.BenderAdminUsers.delete});
    // End Bender Admin User Routes
  // End Bender Admin Routes

  // Customer Admin Routes
    // Namespaced to 

  // Permitted params for user creation/updation
  let customer_admin_user_update_config = {
    validate: {
      payload: {
        email: Joi.string().email(),
        name: Joi.string(),
        username: Joi.string(),
        customer_admin: Joi.boolean()
      }
    }
  };

  server.route({method: 'GET', path: '/users', handler: c.CustomerAdminUsers.index});
  server.route({method: 'GET', path: '/users/new', handler: c.CustomerAdminUsers.new});
  server.route({method: 'POST', path: '/users/new', handler: c.CustomerAdminUsers.create, config: customer_admin_user_update_config});
  server.route({method: 'GET', path: '/users/{username}', handler: c.CustomerAdminUsers.show});
  server.route({method: 'GET', path: '/users/{username}/edit', handler: c.CustomerAdminUsers.edit});
  server.route({method: 'POST', path: '/users/{username}/update', handler: c.CustomerAdminUsers.update, config: customer_admin_user_update_config});

  // Admin Orders
  server.route({method: 'GET', path: '/orders', handler: c.CustomerAdminOrders.index});
  server.route({method: 'GET', path: '/orders/search', handler: c.CustomerAdminOrders.search});
  server.route({method: 'GET', path: '/orders/shipped', handler: c.CustomerAdminOrders.shipped});
  server.route({method: 'GET', path: '/orders/download', handler: c.CustomerReports.orders});
  server.route({method: 'GET', path: '/orders/{order}', handler: c.CustomerAdminOrders.show});
  server.route({method: 'GET', path: '/orders/new', handler: c.CustomerAdminOrders.new});
  server.route({method: 'POST', path: '/orders/new', handler: c.CustomerAdminOrders.create});

  // Admin Inventory
  server.route({method: 'GET', path: '/inventory', handler: c.CustomerAdminInventory.index});
  server.route({method: 'GET', path: '/inventory/{item_id}', handler: c.CustomerAdminInventory.show});
  server.route({method: 'GET', path: '/inventory/download', handler: c.CustomerReports.inventory});
  
  // Api Routes
  server.route({method: 'GET', path: '/api/verify-unique', handler: c.Api.verify_unique});
  server.route({method: 'GET', path: '/api/verify-unique-order', handler: c.Api.verify_unique_order});

  // Notification Routes
  server.route({method: 'GET', path: '/api/notifications', handler: c.Notification.list});
  
  // Database query
  server.route({method: 'POST', path: '/api/query', handler: c.Api.query});
  
  // Address Routes
  server.route({method: 'GET', path: '/addresses', handler: c.Address.index});
  server.route({method: 'GET', path: '/addresses/{id}', handler: c.Address.show});
  server.route({method: 'GET', path: '/addresses/{id}/edit', handler: c.Address.edit});
  server.route({method: 'GET', path: '/addresses/new', handler: c.Address.new});
  
  // Permitted params for user creation/updation
  let address_update_config = {
    validate: {
      payload: {
        _id: Joi.string().allow(''),
        email: Joi.string().email().allow(''),
        phone: Joi.string().allow(''),
        name: Joi.string(),
        line_one: Joi.string(),
        line_two: Joi.string().allow(''),
        line_three: Joi.string().allow(''),
        zip_code: Joi.string(),
        city: Joi.string(),
        state: Joi.string().allow(''),
        country: Joi.string()
      }
    }
  };
  
  let address_delete_config = {
    validate: {
      query: {
        _id: Joi.string()
      }
    }
  };
  
  server.route({method: 'POST', path: '/addresses/new', handler: c.Address.create, config: address_update_config});
  server.route({method: 'POST', path: '/addresses/edit', handler: c.Address.update, config: address_update_config});
  server.route({method: 'GET', path: '/addresses/delete', handler: c.Address.delete, config: address_delete_config});

  // Static files
  server.route({
    method: 'GET',
    path: '/css/{file}.css',
    handler: function (request, reply) {
      reply.file("./public/css/"+request.params.file+".css");
    }
  });
  server.route({
    method: 'GET',
    path: '/js/{file}.js',
    handler: function (request, reply) {
      reply.file("./public/js/"+request.params.file+".js");
    },
    config: {
      cache: {
        expiresIn: 300 * 1000,
        privacy: 'private'
      }
    }
  });
  server.route({
      method: 'GET',
      path: '/images/{file}',
      handler: function (request, reply) {
        reply.file("./public/images/"+request.params.file);
      }
  });
  
   server.route({ method: '*', path: '/{p*}', handler:  function(req, res) {
     res.view('errors/404').code(404);
   }});

   server.route({method: 'GET', path: '/errors/500', handler: (req, res) => {
     res.view('errors/500').code(500);
   }}); 
  
};
