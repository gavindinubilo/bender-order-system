var User = require('./models').User;

// This file is used to authenticate users and to maintain session through site
const bender_admin_route = '/admin';
const customer_admin_route = '/';

module.exports = function(server) {
  // To authenticate the users session
  server.ext('onPreResponse', function (req, reply) {
    const res = req.response;
    if (res.variety !== 'view') return reply.continue();
    if (req.path.indexOf('/verify') > -1) return reply.continue();
    if (req.path.indexOf('/api') > -1) return reply.continue();
    
    const user = req.yar.get('user');
    if (!req.yar.get('user') && req.path === '/login')  return reply.continue();
    if (!req.yar.get('user') && req.path !== '/login')  return reply.redirect('/login');
    
    // Update the user saved in session on every request
    return User.findOne({_id: user._id}, function(err, new_user) {
      if (!new_user){
        req.yar.clear();
        return reply.redirect('/');
      }

      if (!new_user.bender_admin && req.path.indexOf(bender_admin_route) > -1) return reply.redirect('/');
      if (!new_user.account_no && req.path.indexOf(bender_admin_route) === -1) return reply.redirect('/admin');

      if (!res.source.context) {
        res.source.context = {};
      }
    
      if (new_user.need_new_pw && req.path !== '/update-password') {
        return reply.redirect('/update-password');
      }
      res.source.context.current_user = new_user;
      return reply.continue();
    });
  });
}
