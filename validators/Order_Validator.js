var Joi = require('joi');
var async = require('async');

var line_item_schema = {
  bender_item_id: Joi.string().required(),
  customer_item_id: Joi.string(),
  description: Joi.string().min(0).max(500).allow(''),
  quantity: Joi.number().min(1).required()  
};

var address_item_schema = {
  address_type: Joi.string().valid('ST', 'BT', 'FT').required(),
  name: Joi.string().max(100).required(),
  company_name: Joi.string(),
  line_one: Joi.string().max(30).required(),
  line_two: Joi.string().max(30).allow(''),
  line_three: Joi.string().max(30).allow(''),
  city: Joi.string().max(25).required(),
  state: Joi.string().allow('').max(4),
  country: Joi.string().max(10).required(),
  zip_code: Joi.string().required(),
  email: Joi.string().email().max(50).allow(''),
  phone: Joi.string().regex(/[0-9]{3}-[0-9]{3}-[0-9]{4}|[0-9]{10}|\([0-9]{3}\)[0-9]{3}-[0-9]{4}|1-[0-9]{3}-[0-9]{3}-[0-9]{4}|1-\([0-9]{3}\)-[0-9]{3}-[0-9]{4}/).max(30).allow(''),
  fax: Joi.string().regex(/[0-9]{3}-[0-9]{3}-[0-9]{4}|[0-9]{10}|\([0-9]{3}\)[0-9]{3}-[0-9]{4}|1-[0-9]{3}-[0-9]{3}-[0-9]{4}|1-\([0-9]{3}\)-[0-9]{3}-[0-9]{4}/).allow('')
};

var order_schema = {
  customer_number: Joi.string().max(30).required(),
  order_number: Joi.string().max(38).required(),
  po_number: Joi.string().allow(''),
  requested_ship_date: Joi.string().regex(/[0-9]{4}-[0-9]{2}-[0-9]{2}/).required(),
  requested_arrival_date: Joi.string().regex(/[0-9]{4}-[0-9]{2}-[0-9]{2}/).allow(' '),
  carrier_code: Joi.string().max(6),
  payment_method: Joi.string().valid('PP', 'pp', 'CO', 'co', 'TP', 'tp').required(),
  third_party_account_number: Joi.string().allow(''),
  notes: Joi.string().min(0).max(500).allow(''),
  line_items: Joi.array().items(Joi.object().keys(line_item_schema)),
  name: Joi.string().required(),
  email: Joi.string().email().max(50),
  addresses: Joi.array().items(Joi.object().keys(address_item_schema))
};

module.exports.validate = function(data, cb) {
  return Joi.validate(data, order_schema, function(err, value) {
    if (err) {
      return (cb !== undefined) ? cb(err) : false;
    }
    return (cb !== undefined) ? cb(null) : true;
  });
};
