'use strict';

var ODBC = require('odbc')();
var cn = 'DRIVER={Oracle};UID=dev;PASSWORD=dev;'

ODBC.openSync(cn);

var queries = {}

queries.validate_cust_number = function(cust_num) {
  var result = ODBC.querySync("select count(1) from customers where account_no='" + cust_num + "' and customer_status='ACTIVE'");

  return (result[0]['COUNT(1)'] === 1) ? true : false;
}

queries.validate_order_number = function(order_num, cust_num) {
  var result = ODBC.querySync("select count(1) from order_headers where cust_account_no = '"
                            + cust_num + "' and customer_order_no = '"
                            + order_num + "' and order_status <> 'CAN'");

  return (result[0]['COUNT(1)'] === 0) ? true : false;
}

queries.validate_bender_item_id = function(item_id, cust_num) {
  var result = ODBC.querySync("select count(1) from products where account_no='"
                              + cust_num + "' and item_id='"
                              + item_id + "' and complete_flag='Y'");

  return (result[0]['COUNT(1)'] === 1) ? true : false;
}

module.exports = queries;
