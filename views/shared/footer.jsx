'use strict';
const React = require('react');

class Footer extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <footer>
        <div className="wrapper">
          <div className="base-half">
            <div className="base-inner">
              <p>Bender Group is a third party logistics (3PL) company focused on providing intelligent warehousing, transportation and international logistics solutions.</p>
              <p>Copyright 2016 Bender Group</p>
            </div>
          </div>
          <div className="base-half">
            <div className="base-inner">
              <p>
                <a href="http://www.bendergroup.com/terms-of-use/" target="_blank">Terms of Use</a> |
                <a href="http://www.bendergroup.com/">Bender Group</a> |
                <a href="http://www.bendergroup.com/contact/" target="_blank">Contact Us</a>
              </p>
            </div>
          </div>
        </div>        
      </footer>
    )
  }
}

module.exports = Footer;
