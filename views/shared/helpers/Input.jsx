'use strict';
const React = require('react');

class Input extends React.Component {
  constructor(props) {
    super(props);    

    this.state = {
      valid: true,
      awaiting_validation: false,
      message: ''
    }
  }
  update(state) {
    this.setState(state);
    this.setState({awaiting_validation: false});
  }
  onChange() {
    if (this.props.validate) {
      this.setState({awaiting_validation: true});
      this.props.validate(this.refs.input.value, this.update.bind(this));
    }
    if (this.props.onChange) {
      this.props.onChange(this.state);
    }
  }
  onBlur() {    
    if (this.props.validate) {
      this.setState({awaiting_validation: true});
      this.props.validate(this.refs.input.value, this.update.bind(this));
    } 
    if (this.props.onBlur) {
      this.setState({awaiting_validation: true});
      this.props.onBlur(this.refs.input.value, this.update.bind(this));
    } 
  }
  onEnter(e) {
    if (e.charCode === 13 && this.props.onEnter) {
      this.props.onEnter(e);
    }
  }
  renderError() {      
    if (!this.state.valid) {
      return <div className='input-error'>{this.state.message}</div>;
    }
  }
  renderLoader() {
    if (this.state.awaiting_validation) {
      return <div className='loader'></div>
    }
  }
  render() {
    return (
      <div className='input-wrap'>
        <label htmlFor={this.props.name}>{this.props.title}</label>
        {this.renderError()}
        <input 
          type={this.props.type}
          id={this.props.name}
          name={this.props.name}
          ref='input'
          defaultValue={this.props.defaultValue || ''}
          value={this.props.value}
          onBlur={this.onBlur.bind(this)}        
          onChange={this.onChange.bind(this)}
          onKeyPress={this.onEnter.bind(this)}
          autoComplete="off"
        />
        {this.renderLoader()}
      </div>
    )
  }
}

module.exports = Input;