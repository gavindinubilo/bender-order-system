'use strict';
const React = require('react');
const lists = require('./option_lists.js');
const Input = require('./Input.jsx');
const Select = require('./Select.jsx');

class Address extends React.Component {
  constructor(props) {
    super(props);
    var addresses = props.addresses;
    addresses.unshift({name: 'Choose from a saved address.', line_one: ''});
    this.state = {
      country_is_us: true,
      name_error: true,
      email_error: true,
      phone_error: true,
      address_error: true,
      zip_error: true,
      city_error: true,
      state_error: true,
      addresses: addresses,
      name: '', email: '', phone: '', line_one: '', line_two: '', line_three: '', zip_code: '', city: '', state: lists.states[0][0], country: ''      
    }
  }
  onChange() {
    var valid = this.state.name !== '';     
    valid = valid && this.state.line_one !== '';     
    valid = valid && this.state.zip_code !== '';    
    valid = valid && this.state.city !== '';    
    var state_valid = this.state.country_is_us ? this.state.state !== '' : true;
    valid = valid && state_valid;
    var address = {name: this.state.name, email: this.state.email, phone: this.state.phone, line_one: this.state.line_one, line_two: this.state.line_two, line_three: this.state.line_three, zip_code: this.state.zip_code, city: this.state.city, state: this.state.state, country: this.state.country};
    this.props.onChange(address, valid);
  }
  countryChange(country) {
    this.setState({country_is_us: (country === 'US'), state: '', country: country});
    setTimeout(() => {
      this.onChange()
    }, 20);
  }
  renderState() {
    if (this.state.country_is_us) {
      return (
        <Select
          name={`${this.props.prefix}[state]`}
          className='input'
          onChange={(state) => {            
            this.setState({state: state});
            setTimeout(() => {
              this.onChange()
            }, 20);
          }}
          value={this.state.state}
          title={`* ${this.props.name} State`}
          options={lists.states}
          />
      );
    }
    return (
      <Input 
        type='text'
        name={`${this.props.prefix}[state]`}
        id='state'
        title={`${this.props.name} State`}
        value={this.state.state || ''}
        validate={((input, cb) => {
          this.setState({state: input});
          this.onChange();
          if (input.length > 4) {
            return cb({valid: false, message: this.props.name + ' State must be a max of 4 characters.'})
          }
          return cb({valid: true})
        }).bind(this)} />
    )
  }
  renderQuickPick() {
    if (this.props.addresses.length > 0) {
      return (
        <Select
          name=''
          title='Quick Choose'
          options={this.state.addresses}
          optionRenderer={(option, index) => {
            return <option value={index}>{`${option.name} ${option.line_one}`}</option>
          }}
          onChange={(index) => {
            if (index == 0) {
              this.setState({name: '', email: '', phone: '', line_one: '', line_two: '', line_three: '', zip_code: '', city: '', state: '', country: ''});
              return this.onChange();
            }
            this.setState(this.state.addresses[index]);
            this.setState({
              name_error: false,
              email_error: false,
              phone_error: false,
              address_error: false,
              zip_error: false,
              city_error: false,
              state_error: false,
            });
            this.props.onChange(this.state.addresses[index], true);
          }}
          />
      );
    }
  }
  render() {
    return (
      <div>
        <h3>{this.props.name} Address</h3>
        <input type='hidden' name={`${this.props.prefix}[address_type]`} value={this.props.type} />
        {this.renderQuickPick()}
        <Input 
          type='text' 
          name={`${this.props.prefix}[name]`}
          id='name' 
          title={`* ${this.props.name} Name`}
          value={this.state.name}
          onChange={this.onChange.bind(this)}
          validate={((input, cb) => {
            this.setState({name: input});
            this.onChange();
            if (input.trim() === '') {
              this.setState({name_error: true});
              return cb({valid: false, message: this.props.name + ' Name is required.'})
            }
            this.setState({name_error: false});
            return cb({valid: true})
          }).bind(this)} />
        <div className="input--half">
          <Input 
            type='email' 
            name={`${this.props.prefix}[email]`}
            id='email' 
            title={`${this.props.name} Email`}
            value={this.state.email || ''}
            onChange={this.onChange.bind(this)}
            validate={((input, cb) => {
              this.setState({email: input});
              this.onChange();
              if (input.trim() !== '' && !input.match(/[^\s@]+@[^\s@]+\.[^\s@]+/)) {
                this.setState({email_error: true});
                return cb({valid: false, message: this.props.name + ' Email must be a valid email.'})
              }
              this.setState({email_error: false});
              return cb({valid: true})
            }).bind(this)} />
        </div>
        <div className="input--half">
          <Input 
            type='text' 
            name={`${this.props.prefix}[phone]`}
            id='phone' 
            title={`${this.props.name} Phone`}
            value={this.state.phone || ''}
            onChange={this.onChange.bind(this)}
            validate={((input, cb) => {
              this.setState({phone: input});
              this.onChange();
              if (input.trim() !== '' && !input.match(/[0-9]{3}-[0-9]{3}-[0-9]{4}|[0-9]{10}/)) {
                this.setState({phone_error: true});
                return cb({valid: false, message: this.props.name + ' Phone must be formatted correctly (123-456-7890).'})
              }
              this.setState({phone_error: false});
              return cb({valid: true})
            }).bind(this)} />
        </div>        
        <Input
          type='text'
          name={`${this.props.prefix}[line_one]`}
          id='line_one'
          title={`* ${this.props.name} Address Line One`}
          value={this.state.line_one || ''}
          onChange={this.onChange.bind(this)}
          validate={((input, cb) => {
            this.setState({line_one: input});
            this.onChange();
            if (input.trim() === '') {
              this.setState({address_error: true});
              return cb({valid: false, message: this.props.name + ' Address One is required.'})
            }
            this.setState({address_error: false});
            return cb({valid: true})
          }).bind(this)} /> 
        <Input
          type='text'
          name={`${this.props.prefix}[line_two]`}
          id='line_two'
          title={`${this.props.name} Address Line Two`}
          value={this.state.line_two || ''}
          onChange={this.onChange.bind(this)}
          validate={(input, cb) => {
            this.setState({line_two: input});
            this.onChange();
            return cb({valid: true})
          }}
          />          
        <Input
          type='text'
          name={`${this.props.prefix}[line_three]`}
          id='line_three'
          title={`${this.props.name} Address Line Three`}
          value={this.state.line_three || ''}
          onChange={this.onChange.bind(this)}
          validate={(input, cb) => {
            this.setState({line_three: input});
            this.onChange();
            return cb({valid: true})
          }}
          />   
        <div className="input--half">
          <Input
            type='text'
            name={`${this.props.prefix}[city]`}
            id='city'
            title={`* ${this.props.name} City`}
            value={this.state.city || ''}
            onChange={this.onChange.bind(this)}
            validate={((input, cb) => {
              this.setState({city: input});
              this.onChange();
              if (input.trim() === '') {
                this.setState({city_error: true});
                return cb({valid: false, message: this.props.name + ' City is required.'})
              }
              this.setState({city_error: false});
              return cb({valid: true})
            }).bind(this)} /> 
        </div>
        <div className="input--half">
          {this.renderState()}
        </div>
        <div className="input--half">
          <Input
            type='text'
            name={`${this.props.prefix}[zip_code]`}
            id='zip'
            title={`* ${this.props.name} Zip Code`}
            value={this.state.zip_code || ''}
            onChange={this.onChange.bind(this)}
            validate={((input, cb) => {
              this.setState({zip_code: input});
              this.onChange();
              if (input.trim() === '') {
                this.setState({zip_error: true});
                return cb({valid: false, message: this.props.name + ' Zip Code is required.'})
              } else if (input.length > 20) {
                this.setState({zip_error: true});
                return cb({valid: false, message: this.props.name + ' Zip Code must be less than 20 digits'})
              }
              this.setState({zip_error: false});
              return cb({valid: true})
            }).bind(this)} /> 
        </div>
        <div className="input--half">
          <Select
            name={`${this.props.prefix}[country]`}
            className='input'
            title={`* ${this.props.name} Country`}
            value={this.state.country || ''}
            options={lists.countries}
            onChange={this.countryChange.bind(this)}
            />
        </div>
      </div>
    )
  }
}

module.exports = Address;