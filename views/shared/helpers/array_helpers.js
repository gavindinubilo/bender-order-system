'use strict';
Array.prototype.search = function(query) {
  var temp_array = [];
  this.forEach(function(item) {
    for (var key in item) {    
      if (item[key] && item[key].toString().toLowerCase().indexOf(query.toLowerCase()) > -1) {
        temp_array.push(item);
        break;
      }
    }
  });
  if (temp_array.length) return temp_array;
  return this;
}