'use strict';
const React = require('react');

class AddressReview extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div className="review-item">
          <div className="review-item-label">Name</div>
          <div className="review-item-value">{this.props.address.name}</div>
        </div>
        <div className="review-item inner--half">
          <div className="review-item-label">Email</div>
          <div className="review-item-value">{this.props.address.email}</div>
        </div>
        <div className="review-item inner--half">
          <div className="review-item-label">Phone</div>
          <div className="review-item-value">{this.props.address.phone}</div>
        </div>
        <div className="review-item">
          <div className="review-item-label">Line One</div>
          <div className="review-item-value">{this.props.address.line_one}</div>
        </div>
        <div className="review-item">
          <div className="review-item-label">Line Two</div>
          <div className="review-item-value">{this.props.address.line_two}</div>
        </div>
        <div className="review-item">
          <div className="review-item-label">Line Three</div>
          <div className="review-item-value">{this.props.address.line_three}</div>
        </div>
        <div className="review-item inner--half">
          <div className="review-item-label">Zip Code</div>
          <div className="review-item-value">{this.props.address.zip_code}</div>
        </div>
        <div className="review-item inner--half">
          <div className="review-item-label">City</div>
          <div className="review-item-value">{this.props.address.city}</div>
        </div>
        <div className="review-item inner--half">
          <div className="review-item-label">State</div>
          <div className="review-item-value">{this.props.address.state}</div>
        </div>
        <div className="review-item inner--half">
          <div className="review-item-label">Country</div>
          <div className="review-item-value">{this.props.address.country}</div>
        </div>
      </div>
    );
  }
}

module.exports = AddressReview;