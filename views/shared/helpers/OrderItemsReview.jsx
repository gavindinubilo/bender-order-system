'use strict';
const React = require('react');

class OrderItemsReview extends React.Component {
  constructor(props) {
    super(props);
  }
  renderItem(item) {
    return (
      <div>
        <div className="review-item inner--third">
          <div className="review-item-label">Item Id</div>
          <div className="review-item-value">{item.ITEM_ID}</div>
        </div>
        <div className="review-item inner--third">
          <div className="review-item-label">Description</div>
          <div className="review-item-value">{item.ITEM_DESCR_1}</div>
        </div>
        <div className="review-item inner--third">
          <div className="review-item-label">Quantity</div>
          <div className="review-item-value">{item.quantity}</div>
        </div>
      </div>
    );
  }
  render() {
    if (!this.props.items) return <div></div>;
    var items = this.props.items.map(function(item, index) {
      return (this.renderItem(item))
    }.bind(this));
    return (
      <div>{items}</div>
    );
  }
}

module.exports = OrderItemsReview;