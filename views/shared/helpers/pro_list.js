module.exports = [
  {
    name: 'ABF Freight INC.',
    link: 'https://www.abfs.com/tools/trace/default.asp?hidSubmitted=Y&refno0=XXX&reftype0=A',
    keys: ['ABF']
  },
  {
    name: 'Central Freight',
    link: 'http://www.centralfreight.com/website/mf/mfInquiry.aspx?inqmode=PRO&pro=XXX',
    keys: ['CENF']
  },
  {
    name: 'Con-Way Western',
    link: 'http://www.con-way.com/webapp/manifestrpts_p_app/Tracking/TrackingRS.jsp?&PRO=XXX',
    keys: ['CWWE', 'CNWY']
  },
  {
    name: 'Estes',
    link: 'http://www.estes-express.com/cgi-dta/edn419.mbr/output?search_criteria=XXX',
    keys: ['ESTS', 'EXLA']
  },
  {
    name: 'SAIA',
    link: 'http://www.saiasecure.com/tracing/b_manifest.asp?&link=y&pro=XXX',
    keys: ['SAIA']
  },
  {
    name: 'VITRAN',
    link: 'http://www.vitranexpress.com/Tracing/Tracing2.aspx?epr=XXX',
    keys: ['VITRAN']
  },
  {
    name: 'YRC',
    link: 'http://my.yrc.com/dynamic/national/servlet?CONTROLLER=com.rdwy.ec.rextracking.http.controller.ProcessPublicTrackingController&PRONumber=XXX',
    keys: ['YRC', 'RDWY']
  },
  {
    name: 'USF Reddaway',
    link: 'http://www.usfc.com/shipmentStatus/track.do?proNumber=XXX',
    keys: ['RETL']
  },
  {
    name: 'UPS',
    link: 'https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=XXX',
    keys: ['UPS1A', 'UPSC', 'UPS1', 'UPS2', 'UPS3', 'UPSG', 'UPSH', 'UPSF', 'UPSA', 'UPSM']
  },
  {
    name: 'FedEx',
    link: 'https://www.fedex.com/apps/fedextrack/?action=track&action=track&tracknumbers=XXX',
    keys: ['FDX2', 'FDXF', 'FDGH', 'FDGR', 'FDX3', 'FDEP', 'FDX1', 'FDXR', 'FDXG', 'FDEG', 'FDEF', 'FDIP', 'FDIE', 'FDX4', 'FXIE', 'FDXH']
  },
  {
    name: 'OnTrac',
    link: 'http://www.ontrac.com/trackingdetail.asp?tracking=XXX',
    keys: ['ONTR']
  },
  {
    name: 'DHLeC',
    link: 'http://webtrack.dhlglobalmail.com/?trackingnumber=XXX',
    keys: ['DHLEC']
  }
]