'use strict';
const React = require('react');
const Input = require('./Input.jsx');
const ZeroState = require('./zero_state.jsx');
const _ = require('underscore');

class OrderItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shipped_items: [],
      unshipped_items: props.products,
      page: 0,
      per_page: 5,
      search: '',
      modal_product: null,
      direct_item: null,
      direct_item_id: ''
    }
  }
  shippedLength() {
    return this.state.shipped_items.length;
  }
  addToShippedItems(product) {
    var temp_shipped_items = this.state.shipped_items;
    temp_shipped_items.push(product);
    this.setState({shipped_items: temp_shipped_items});
    this.props.update(this.state.shipped_items, this.state.shipped_items.length !== 0);
  }
  removeFromShippedItems(product) {
    var temp_shipped_items = this.state.shipped_items;
    const index = temp_shipped_items.indexOf(product);
    temp_shipped_items.splice(index, 1);
    this.setState({shipped_items: temp_shipped_items});
    this.props.update(this.state.shipped_items, this.state.shipped_items.length !== 0);
  }
  onDirectAddClicked(e) {
    if (e) e.preventDefault();
    if (!this.directAddValid()) return;
    var item = this.state.direct_item;
    item.quantity = 1;
    this.addToShippedItems(item);
    this.setState({direct_item: null, direct_item_id: ''});
  }
  directAddValid() {
    return this.state.direct_item != null;
  }
  searchChange() {
    this.setState({search: this.refs.search.value, page: 0});
  }
  filterProducts() {
    const products = this.props.products.search(this.state.search);
    const unshipped = _.difference(products, this.state.shipped_items);
    const filter_zero_on_hand = unshipped.filter((product) => {
      if (product.ON_HAND_QTY > 0) return product;
    });    

    return filter_zero_on_hand;
  }
  increaseProductQuantity(product) {
    product.quantity++;
  }
  decreaseProductQuantity(product) {
    product.quantity--;
  }
  renderShippedItems() {
    if (this.state.shipped_items.length) {
      const shipped_items = this.state.shipped_items.map((product, index) => {
        return (
          <div className='row-item'>
            <div className="attribute action small" onClick={this.removeFromShippedItems.bind(this, product)}>
              <i className="fa fa-2x fa-minus"></i>
            </div>
            <div className="attribute">
              {product.ITEM_ID}
            </div>
            <div className="attribute">
              {product.ITEM_DESCR_1}
            </div>
            <div className="attribute medium quantity-buttons">
              <button onClick={(e) => {
                e.preventDefault();
                this.decreaseProductQuantity(product);
                this.setState({shipped_items: this.state.shipped_items});
              }} disabled={product.quantity === 1}>-</button>
              {product.quantity}
              <button onClick={(e) => {
                e.preventDefault();
                this.increaseProductQuantity(product);
                this.setState({shipped_items: this.state.shipped_items});
              }}>+</button>
              </div>        
            <input type='hidden' name={`line_items[${index}][description]`} value={product.description} />
            <input type='hidden' name={`line_items[${index}][quantity]`} value={product.quantity} />
            <input type='hidden' name={`line_items[${index}][bender_item_id]`} value={product.ITEM_ID} />
          </div>
        );
      });
      return (
        <div>
          <div className='row-heading'>
            <div className="attribute small">
            </div>
            <div className="attribute">
              ID
            </div>
            <div className="attribute">
              Description
            </div>
            <div className="attribute medium">
              Quantity
            </div>
          </div>
          {shipped_items}
        </div>
      );
    }
    return (
      <ZeroState>
        You haven't added any items yet.
      </ZeroState>
    );
  }
  renderUnShippedItems() {
    const temp_products = this.filterProducts()

    const temp_inventory = temp_products.slice((this.state.page * this.state.per_page), ((this.state.page * this.state.per_page) + this.state.per_page));

    const unshipped_items = temp_inventory.map((product, index) => {
      if (this.state.shipped_items.indexOf(product) > -1) return;
      return (
        <div className='row-item'>
          <div className="attribute action small" onClick={(e) => {
              product.quantity = 1;
              this.addToShippedItems(product);
            }}>
            <i className="fa fa-2x fa-plus"></i>
          </div>
          <div className="attribute">
            {product.ITEM_ID}
          </div>
          <div className="attribute">
            {product.ITEM_DESCR_1}
          </div>
          <div className="attribute medium">
            {product.ON_HAND_QTY}
          </div>        
        </div>
      );
    });
    return unshipped_items;
  }
  renderPrevButton() {
    if (this.state.page !== 0) {
      return (
        <a onClick={() => {this.setState({page: this.state.page - 1})}}>
          <i className="fa fa-chevron-left"></i>
          Prev
        </a>
      );
    }
  }
  renderNextButton() {
    const unshipped = this.filterProducts();
    if (this.state.page < ((unshipped.length) / this.state.per_page) - 1) {
      return (
        <a onClick={() => {this.setState({page: this.state.page + 1})}}>
          Next
          <i className="fa fa-chevron-right"></i>
        </a>
      );
    }
  }
  renderModalProduct() {
  }
  render() {
    return (
      <div>        
        {this.renderShippedItems()}
        <h3>Add Directly</h3>
        <div className="form--inline">
          <Input 
            type='text' 
            name='' 
            id='' 
            ref='' 
            title='Item ID'
            value={this.state.direct_item_id}
            onEnter={this.onDirectAddClicked.bind(this)}
            validate={((input, cb) => {
              this.setState({direct_item_id: input});
              if (input.trim() === '') {
                return cb({valid: true, message: ''});
              }
              var item = this.filterProducts().find((item) => {
                return item.ITEM_ID === input;
              });
              if (!item) {
                return cb({valid: false, message: 'Couldn\'t find an item with the given ID'});  
              }
              this.setState({direct_item: item});
              return cb({valid: true});
            })} />          
          <button className='btn' disabled={!this.directAddValid()} onClick={this.onDirectAddClicked.bind(this)}>Add</button>
        </div>
        <h3>Select From List</h3>
        <div className='row-heading'>
          <div className="attribute small">
          </div>
          <div className="attribute">
            ID
          </div>
          <div className="attribute">
            Description
          </div>
          <div className="attribute medium">
            On Hand
          </div>
        </div>        
        <div className="row">
          <input type='text' ref='search' placeholder='Search Items' onChange={this.searchChange.bind(this)} />
        </div>       
        {this.renderUnShippedItems()}
        <div className="pagination">
          <div className="prev-wrapper">
            {this.renderPrevButton()}
          </div>
          <div className="next-wrapper">
            {this.renderNextButton()}
          </div>
        </div>
      </div>
    );
  }
}

module.exports = OrderItems;