'use strict';
const React = require('react');
const pro_list = require('./pro_list.js');

class ProLink extends React.Component {
  constructor(props) {
    super(props);
  }
  href() {
    for (var scac of pro_list) {
      if (scac.keys.indexOf(this.props.scac) > -1) {
        return scac.link.replace('XXX', this.props.pro_no);
      }
    }
    return '#';
  }
  text() {
    for (var scac of pro_list) {
      if (scac.keys.indexOf(this.props.scac) > -1) {
        return `${scac.name} - ${this.props.pro_no}`;
      }
    }
    return '-';
  }
  render() {
    if (this.props.pro_no === '') return '-';
    return (
      <a href={this.href()} target='_blank'>
        {this.text()}
      </a>   
    );
  }
}

module.exports = ProLink;