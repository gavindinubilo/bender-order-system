'use strict';
const React = require('react');

class ZeroState extends React.Component {
  render() {
    return (
      <div className='zero-state'>
        <i className="fa fa-2x fa-exclamation-circle"></i>
        {this.props.children} 
      </div>
    )
  }
}

module.exports = ZeroState;