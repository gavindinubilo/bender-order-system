'use strict';
const React = require('react');

class Select extends React.Component {
  constructor(props) {
    super(props);
  }
  renderOptions() {
    const options = this.props.options.map(((option, index) => {
      if (this.props.optionRenderer) {
        return this.props.optionRenderer(option, index);
      }
      return (
        <option value={option[0]} key={index}>{option[1]}</option>
      );
    }));
    return options;
  }
  onChange() {
    if (this.props.onChange) {
      this.props.onChange(this.refs.select.value);
    }
  }
  render() {
    return (
      <div>
        <label htmlFor={this.props.name}>{this.props.title}</label>
        <select ref='select' className={this.props.className || ''} value={this.props.value} name={this.props.name} onChange={this.onChange.bind(this)}>
          {this.renderOptions()}
        </select>
      </div>
    );
  }
}

module.exports = Select;