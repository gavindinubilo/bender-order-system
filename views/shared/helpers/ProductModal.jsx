'use strict';
const React = require('react');

class ProductModal extends React.Component {
  constructor(props) {
    super(props);
  }
  onSubmit(e) {
    e.preventDefault();
    var temp_product = this.props.product;
    temp_product.description = this.refs.description.value;
    temp_product.quantity = this.refs.quantity.value;

    this.props.onSubmit(temp_product);
  }
  render() {
    return (
      <div>
        <div className="modal-bg"></div>  
        <div className='modal'>
          <div className="modal-header">
            <h1>Add Product</h1>
          </div>
          <p>Item ID: {this.props.product.ITEM_ID}</p>
          <label htmlFor="description">Description</label>
          <textarea ref='description' id='description'></textarea>
          <label htmlFor="quantity">Quantity</label>
          <input ref='quantity' type='number' placeholder='Quantity' id='quantity' defaultValue='1' min='1' max={this.props.product.ON_HAND_QTY} />
          <button className="btn" onClick={this.onSubmit.bind(this)}>Submit</button>
          <button className='btn btn--error' onClick={(e) => {this.props.onCancel()}}>Cancel</button>
        </div>
      </div>
    );
  }
}

module.exports = ProductModal;