'use strict';

const React = require('react');

class AdminNavigation extends React.Component {
  constructor(props) {
    super(props);
  }
  renderCustomerLinks() {
    if (this.props.current_user.customer_admin) {
      return (
        <div>
          <a href="/admin">
            <i className="fa fa-home"></i>
            Home
          </a>
          <a href="/admin/orders">
            <i className="fa fa-shopping-basket"></i>
            Orders
          </a>
          <a href="/admin/users">
            <i className="fa fa-users"></i>
            Users
          </a>
          <a href='/admin/inventory'>
            <i className="fa fa-dropbox"></i>
            Inventory
          </a>  
        </div>
      );
    }
  }
  renderAdminLinks() {
    if (this.props.current_user.bender_admin) {
      return (
        <div>
          <a href="/">
            <i className="fa fa-home"></i>
            Home
          </a>
          <a href="/orders">
            <i className="fa fa-shopping-basket"></i>
            Orders
          </a>
          <a href="/users">
            <i className="fa fa-users"></i>
            Users
          </a>
          <a href='/inventory'>
            <i className="fa fa-dropbox"></i>
            Inventory
          </a>  
        </div>
      );
    }
  }
  render() {
    return (
      <div className="admin-nav">
        <div className="wrapper">
          {(() => {
            if (!this.props.current_user.customer_admin) return;
            return (
              <a href="/">
                <i className="fa fa-home"></i>
                Home
              </a>
            );
          })()}
          {(() => {
            if (!this.props.current_user.customer_admin) return;
            return (
              <a href="/orders">
                <i className="fa fa-shopping-basket"></i>
                Orders
              </a>
            );
          })()}
          {(() => {
            if (!this.props.current_user.customer_admin) return;
            return (
              <a href="/users">
                <i className="fa fa-users"></i>
                Users
              </a>
            );
          })()}
          {(() => {
            if (!this.props.current_user.customer_admin) return;
            return (
              <a href='/inventory'>
                <i className="fa fa-dropbox"></i>
                Inventory
              </a>  
            );
          })()}
          {(() => {
            if (!this.props.current_user.bender_admin) return;
            return (
              <a href="/admin">
                <i className="fa fa-bolt"></i>
                Admin
              </a>
            );
          })()}
          {(() => {
            if (!this.props.current_user.bender_admin) return;
            return (
              <a href="/admin/users">
                <i className="fa fa-users"></i>
                All Users
              </a>
            );
          })()}
        </div>
      </div>
    )
  }
}

module.exports = AdminNavigation;
