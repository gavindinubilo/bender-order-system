'use strict';
const React = require('react');

class Navigation extends React.Component {
  constructor(props) {
    super(props);
  }  
  render() {   
    return (
      <nav>
        <div className="wrapper">
          <a href="/" className="logo">
            <img src="/images/logo.png" alt="Bender Group Logo"/>
          </a>
          <div className="right">
            {(() => {
              if (!this.props.current_user || !this.props.current_user.account_no) return;
              return (
                <a href="/">                  
                  Home
                </a>
              );
            })()}
            {(() => {
              if (!this.props.current_user || !this.props.current_user.account_no) return;
              if (!this.props.current_user.customer_admin) return (
                <div className='link drop'>                  
                  <span>
                    Orders
                    <i className="fa fa-chevron-down"></i>
                  </span>                  
                  <div className="drop-menu">
                    <a href="/orders">Open Orders</a>
                    <a href="/orders/shipped">Shipped Orders</a>
                  </div>
                </div>
              )
              return (
                <div className='link drop'>                  
                  <span>
                    Orders
                    <i className="fa fa-chevron-down"></i>
                  </span>                  
                  <div className="drop-menu">
                    <a href="/orders/new">New Order</a>
                    <a href="/orders">Open Orders</a>
                    <a href="/orders/shipped">Shipped Orders</a>
                  </div>
                </div>
              );
            })()}
            {(() => {
              if (!this.props.current_user || !this.props.current_user.account_no) return;
              return (
                <a href='/inventory'>                  
                  Inventory
                </a>  
              );
            })()}
            {(() => {
              if (!this.props.current_user || !this.props.current_user.bender_admin) return;
              if (this.props.current_user.account_no) return (
                <div className='link drop'>
                  <span>Admin <i className="fa fa-chevron-down"></i></span>
                  <div className="drop-menu">
                    <a href="/admin">                  
                      Admin
                    </a>
                    <a href="/admin/users">                  
                      All Users
                    </a>
                  </div>
                </div>
              );
              return (
                <div>
                  <a href="/admin">                  
                    Admin
                  </a>
                  <a href="/admin/users">                  
                    All Users
                  </a>
                </div>
              );
            })()}            
            {(() => {
              if (!this.props.current_user) return;
              if (!this.props.current_user.customer_admin) return (
                <a href="/logout" className="link">                  
                  Logout
                </a>
              )
              return (
                <div className='link drop drop--right'>                  
                  <span>
                    More
                    <i className="fa fa-chevron-down"></i>
                  </span>    
                  <div className="drop-menu">
                    <a href='/addresses'>                  
                      Addresses
                    </a>
                    <a href="/logout" className="link">                  
                      Logout
                    </a>
                  </div>
                </div>
              );
            })()}
          </div>
        </div>
      </nav>
    )
  }
}

module.exports = Navigation;
