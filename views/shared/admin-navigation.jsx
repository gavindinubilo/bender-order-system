'use strict';
const React = require('react');

class AdminNavigation extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="admin-nav">
        <div className="wrapper">
          <a href="/admin">
            <i className="fa fa-home"></i>
            Bender Admin
          </a>
          <a href="/admin/users">
            <i className="fa fa-users"></i>
            Users
          </a>
        </div>
      </div>
    );
  }
}

module.exports = AdminNavigation;
