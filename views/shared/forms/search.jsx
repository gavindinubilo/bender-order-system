'use strict';
const React = require('react');
const Input = require('../helpers/Input.jsx');
const Select = require('../helpers/Select.jsx');
 
class OrderSearch extends React.Component {
  constructor() {
    super();
    this.state = {
      advanced: false
    };
  }
  renderAdvanced() {
    if (!this.state.advanced) return;
    return (
      <div>
        <div className="input--half">
          <Input type='text' name='bol_no' title='BOL Number' />
        </div>
        <div className="input--half">
          <Select
            name='carrier_code'
            title='Carrier Code'
            options={this.props.carrier_codes}
            className='input'
            optionRenderer={(option, index) => {
              return (
                <option value={option.SCAC_CODE} key={index}>{option.NAME}</option>
              )
            }}
          />
        </div> 
        <div className="input--half">
          <Input type='text' name='po_number' title='P.O. Number' />
        </div>
        <div className="input--half">
          <Input type='text' name='pro_no' title='Tracking Number' />
        </div>
        <Input type='text' name='ship_to_address' title='Ship To Address' />
        <div className="input--half">
          <Input 
            type='text' 
            name='order_date_start' 
            title='Order Date (Start)'
            validate={(new_date, cb) => {
              if (new_date.length === 10 && new_date.match(/(0[1-9]|1[012])[- \-.](0[1-9]|[12][0-9]|3[01])[- \-.](19|20)\d\d/)) {
                return cb({valid: true, message: ''});  
              }
              return cb({valid: false, message: 'Please format the date in the following format: MM-DD-YYYY'});
            }}
            />  
        </div>
        <div className="input--half">
          <Input 
            type='text' 
            name='order_date_end' 
            title='Order Date (End)'
            validate={(new_date, cb) => {
              if (new_date.length === 10 && new_date.match(/(0[1-9]|1[012])[- \-.](0[1-9]|[12][0-9]|3[01])[- \-.](19|20)\d\d/)) {
                return cb({valid: true, message: ''});  
              }
              return cb({valid: false, message: 'Please format the date in the following format: MM-DD-YYYY'});
            }}
            />
        </div>
        <div className="input--half">
          <Input 
            type='text' 
            name='ship_date_start' 
            title='Ship Date (Start)'
            validate={(new_date, cb) => {
              if (new_date.length === 10 && new_date.match(/(0[1-9]|1[012])[- \-.](0[1-9]|[12][0-9]|3[01])[- \-.](19|20)\d\d/)) {
                return cb({valid: true, message: ''});  
              }
              return cb({valid: false, message: 'Please format the date in the following format: MM-DD-YYYY'});
            }}
            />  
        </div>
        <div className="input--half">
          <Input 
            type='text' 
            name='ship_date_end' 
            title='Ship Date (End)'
            validate={(new_date, cb) => {
              if (new_date.length === 10 && new_date.match(/(0[1-9]|1[012])[- \-.](0[1-9]|[12][0-9]|3[01])[- \-.](19|20)\d\d/)) {
                return cb({valid: true, message: ''});  
              }
              return cb({valid: false, message: 'Please format the date in the following format: MM-DD-YYYY'});
            }}
            />  
        </div>
      </div>
    );
  }
  render() {
    return (
      <form action='/orders/search' method='get'>
        <input type='text' name='order_no' placeholder='Order Number' />
        <div>
          {(() => {
            if (this.state.advanced) return;
            return (
              <div className="right">
                <button type='button' className='btn btn--small btn--clear' onClick={() => {
                  this.setState({advanced: true});
                }}>
                  Advanced Search
                </button>
              </div>
            );
          })()}
          {this.renderAdvanced()}
          <input type='submit' value='Search' className='btn btn--small btn--inline' />                           
        </div>
      </form>
    );
  }
}

module.exports = OrderSearch;
