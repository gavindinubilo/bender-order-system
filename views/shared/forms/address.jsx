'use strict';
const React = require('react');
const Input = require('../helpers/Input.jsx');
const Select = require('../helpers/Select.jsx');
const lists = require('../helpers/option_lists.js');

class AddressForm extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      country_is_us: true,
      name_error: true,
      email_error: false,
      phone_error: false,
      line_one_error: true,
      zip_code_error: true,
      city_error: true,
      state_error: false
    }
  }
  componentWillMount() {
    if (this.props !== {}) {
      this.setState({
        name_error: false,
        email_error: false,
        phone_error: false,
        line_one_error: false,
        zip_code_error: false,
        city_error: false,
        state_error: false
      });
    }
  }
  formValid() {
    return this.state.name_error || this.state.email_error || this.state.phone_error || this.state.line_one_error || this.state.zip_code_error || this.state.city_error || this.state.state_error;
  }
  renderState() {
    if (this.state.country_is_us) {
      return (
        <Select
          name={`state`}
          className='input'
          defaultValue={this.props.state || ''}
          onChange={(state) => {
            this.setState({state: state});
          }}
          title={`* State`}
          options={lists.states}
          />
      );
    }
    return (
      <Input 
        type='text'
        name={`state`}
        id='state'
        title={`* State`}
        defaultValue={this.props.state}
        validate={((input, cb) => {
          if (input.length > 4) {
            this.setState({state_error: true});
            return cb({valid: false, message: 'State must be a max of 4 characters.'})
          }
          this.setState({state_error: false});
          return cb({valid: true})
        })} />
    )
  }
  confirmDelete(e) {
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: `You will not be able to recover this address!`,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DF4848",
      confirmButtonText: "Yes, delete!",
      closeOnConfirm: false,
      html: false
    }, function(){
      window.location = `/addresses/delete?_id=${this.props._id}`;
    }.bind(this));  
  }
  renderDeleteButton() {
    if (this.props.name === undefined) return;
    return (
      <button className='error' onClick={this.confirmDelete.bind(this)}>Delete</button>
    );
  }
  render() {
    return (
      <div>
        <Input
          type='text'
          name='name'
          title='* Name'
          defaultValue={this.props.name || ''}
          validate={(input, cb) => {
            if (input.trim() === '') {
              this.setState({name_error: true});
              return cb({valid: false, message: 'Name is required'})
            }
            this.setState({name_error: false});
            return cb({valid: true});
          }}
          />
        <div className="input--half">
          <Input 
            type='email' 
            name={`email`}
            id='email' 
            title={`Email`}
            defaultValue={this.props.email || ''}
            validate={(input, cb) => {
              if (input.trim() !== '' && !input.match(/[^\s@]+@[^\s@]+\.[^\s@]+/)) {
                this.setState({email_error: true});
                return cb({valid: false, message: 'Email must be a valid email.'})
              }
              this.setState({email_error: false});
              return cb({valid: true})
            }} />
        </div>
        <div className="input--half">
          <Input 
            type='text' 
            name={`phone`}
            id='phone' 
            title={`Phone`}
            defaultValue={this.props.phone || ''}
            validate={(input, cb) => {
              if (input.trim() !== '' && !input.match(/[0-9]{3}-[0-9]{3}-[0-9]{4}|[0-9]{10}/)) {
                this.setState({phone_error: true});
                return cb({valid: false, message: 'Phone must be formatted correctly (123-456-7890).'})
              }
              this.setState({phone_error: false});
              return cb({valid: true})
            }} />
        </div>
        <Input
          type='text'
          name={`line_one`}
          id='line_one'
          title={`* Address Line One`}
          defaultValue={this.props.line_one || ''}
          validate={(input, cb) => {
            if (input.trim() === '') {
              this.setState({line_one_error: true});
              return cb({valid: false, message: 'Address One is required.'})
            }
            this.setState({line_one_error: false});
            return cb({valid: true})
          }} />
        <Input
          type='text'
          name={`line_two`}
          id='line_two'
          title={`Address Line Two`}
          defaultValue={this.props.line_two || ''}
          validate={(input, cb) => {
            return cb({valid: true})
          }}
          />          
        <Input
          type='text'
          name={`line_three`}
          id='line_three'
          title={`Address Line Three`}
          defaultValue={this.props.line_three || ''}
          validate={(input, cb) => {
            return cb({valid: true})
          }}
          />
        <div className="input--half">
          <Input
            type='text'
            name={`zip_code`}
            id='zip'
            title={`* Zip Code`}
            defaultValue={this.props.zip_code || ''}
            validate={(input, cb) => {
              if (input.trim() === '') {
                this.setState({zip_code_error: true});
                return cb({valid: false, message: 'Zip Code is required.'})
              } else if (!input.match(/[0-9]/) || input.length !== 5) {
                this.setState({zip_code_error: true});
                return cb({valid: false, message: 'Zip Code must be formatted as a 5 digit number'})
              }
              this.setState({zip_code_error: false});
              return cb({valid: true})
            }} /> 
        </div>
        <div className="input--half">
          <Input
            type='text'
            name={`city`}
            id='city'
            title={`* City`}
            defaultValue={this.props.city || ''}
            validate={(input, cb) => {
              if (input.trim() === '') {
                this.setState({city_error: true});
                return cb({valid: false, message: 'City is required.'})
              }
              this.setState({city_error: false});
              return cb({valid: true})
            }} /> 
        </div>
        <div className="input--half">
          {this.renderState()}
        </div>
        <div className="input--half">
          <Select
            name={`country`}
            className='input'
            title={`* Country`}
            onChange={(value) => {
              this.setState({country_is_us: value === 'US'});
            }}
            defaultValue={this.props.country || 'US'}
            options={lists.countries}
            />
        </div>
        <input type="submit" value="Submit" disabled={this.formValid()}/>
        {this.renderDeleteButton()}
      </div>
    );
  }
}

module.exports = AddressForm;