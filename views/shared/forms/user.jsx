'use strict';
const React = require('react');

class UserForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      submittable: false,
      username_error: false,
      email_error: false
    }
  }
  componentDidMount() {
    for (var key in this.props) {
      if (this.refs[key]) {
        if (this.refs[key].type === 'checkbox') {
          this.refs[key].checked = JSON.parse(this.props[key]);
        } else {
          this.refs[key].value = this.props[key];
        }
      }
    }
    this.onChange();
  }
  onChange() {
    if (this.refs.username.value !== '' &&
        this.refs.email.value !== '' &&
        (this.props.current_user.bender_admin && this.refs.account_no.value) !== '' &&
        !this.state.username_error && !this.state.email_error) {
      return this.setState({submittable: true});
    }
    this.setState({submittable: false});
  }
  onUniqueChange(state_name, e) {
    if (e.target.value === this.props[state_name]) return;
    fetch(`/api/verify-unique?type=${state_name}&value=${e.target.value}`)
      .then(function(response) {
        return response.text();
      }).then(function(text) {
        const new_state = {}
        new_state[`${state_name}_error`] = false;
        if (text !== 'ok') {
          new_state[`${state_name}_error`] = true;
        }
        this.setState(new_state);
        this.onChange();
      }.bind(this));
  }
  showError(name) {
    if (this.state[`${name}_error`]) {
      return <div className='input-error'>A user with this {name} already exists</div>;
    }
  }
  confirmDelete(e) {
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: `You will not be able to recover ${this.props.username}!`,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DF4848",
      confirmButtonText: "Yes, delete!",
      closeOnConfirm: false,
      html: false
    }, function(){
      window.location = '/admin/users/'+ this.props.username + '/delete';
    }.bind(this));
  }
  getDeleteUrl() {

  }
  getPostUrl() {
    var url = '';
    if (this.props.current_user && this.props.current_user.bender_admin) {
      url = url + '/admin/users';
    } else {
      url = url + '/users';
    }

    if (this.props.username === undefined) return url + '/new';

    return `${url}/${this.props.username}/update`;
  }
  renderDeleteButton() {
    if (this.props.username === undefined) return;
    return (
      <button className='error' onClick={this.confirmDelete.bind(this)}>Delete</button>
    )
  }
  renderAdminCheckBoxes() {
    if (this.props.current_user && this.props.current_user.bender_admin) {
      return (
        <div>
          <label htmlFor='account_no'>Account Number</label>
          <input type='text' name='account_no' id='account_no' ref='account_no' onChange={this.onChange.bind(this)} />
          <label className='checkbox'>Bender Admin <input type='checkbox' name='bender_admin' ref='bender_admin' onChange={this.onChange.bind(this)} /></label>
          <label className='checkbox'>Customer Admin <input type='checkbox' name='customer_admin' ref='customer_admin' onChange={this.onChange.bind(this)} /></label>
        </div>
      )
    }
    return (
      <label className='checkbox'>Admin <input type='checkbox' name='customer_admin' ref='customer_admin' onChange={this.onChange.bind(this)} /></label>
    );
  }
  render() {
    return (
      <form onSubmit={this.beforeSubmit} action={this.getPostUrl()} method='post'>
        <label htmlFor='username'>Username</label>
        {this.showError('username')}
        <input type='text' ref='username'
          name='username' id='username'
          autoComplete='off'
          onChange={this.onUniqueChange.bind(this, 'username')} />
        <label htmlFor='name'>Full Name</label>
        <input type='text' name='name' id='name' ref='name' onChange={this.onChange.bind(this)} autoComplete='off' />
        <label htmlFor='email'>Email</label>
        {this.showError('email')}
        <input type='email' name='email' id='email' ref='email'
            autoComplete='off'
          onChange={this.onUniqueChange.bind(this, 'email')} />
        {this.renderAdminCheckBoxes()}
        <input type='submit' disabled={!this.state.submittable} />
        {this.renderDeleteButton()}
      </form>
    )
  }
}

module.exports = UserForm;
