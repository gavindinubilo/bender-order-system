'use strict';
const React = require('react');
const moment = require('moment');
require('twix');
const DatePicker = require('react-date-picker');
const lists = require('../helpers/option_lists.js');
const _ = require('underscore');
require('../helpers/string_helpers.js');
require('../helpers/array_helpers.js');

const Input = require('../helpers/Input.jsx');
const Select = require('../helpers/Select.jsx');
const Address = require('../helpers/Address.jsx');
const AddressReview = require('../helpers/AddressReview.jsx');
const OrderItemsReview = require('../helpers/OrderItemsReview.jsx');
const ProductModal = require('../helpers/ProductModal.jsx');
const OrderItems = require('../helpers/OrderItems.jsx');

class OrderForm extends React.Component {
  constructor(props) {
    super(props);
    var carrier_codes = props.carrier_codes;
    carrier_codes.unshift({SCAC_CODE: '', NAME: 'Choose One'});
    this.state = {
      active_step: 1,
      steps_validate: [],
      order_number_error: true,
      scac_number_error: true,
      addresses: {},
      shipping_address_error: true,
      billing_address: false,      
      billing_address_error: false,
      freight_address: false,
      freight_address_error: false,
      order_items_valid: false,
      requested_ship_date: moment().format('YYYY-MM-DD'),
      requested_arrival_date: '',
      carrier_codes: carrier_codes,
      freight_method: 'pp'
    }
  }
  activeStep(index) {
    return this.state.active_step === index;
  }
  groupClicked(index) {
    return;
  }
  validateDate() {
    var ship_date = moment(this.state.requested_ship_date).format('MM-DD-YYYY');
    var date_valid = (this.state.requested_ship_date !== '' && ship_date.match(/(0[1-9]|1[012])[- \-.](0[1-9]|[12][0-9]|3[01])[- \-.](19|20)\d\d/));
    if (this.state.requested_arrival_date !== '') {
      var formed_ship_date = moment(this.state.requested_ship_date).toDate();
      var formed_arrival_date = moment(this.state.requested_arrival_date).toDate();
      var diff = formed_arrival_date - formed_ship_date;
      date_valid = date_valid && diff > 0;
    }
    return date_valid;
  }
  validateFirstSection() {
    return this.validateDate() && !this.state.order_number_error && !this.state.scac_number_error;
  }
  validateSecondSection() {
    return !this.state.shipping_address_error && !this.state.billing_address_error && !this.state.freight_address_error;
  }
  validateThirdSection() {
    return this.state.order_items_valid;
  }
  validateSection(index) {
    switch (index) {
      case 1: return this.validateFirstSection();
      case 2: return this.validateSecondSection();
      case 3: return this.validateThirdSection();
    }
  }
  goToStep(index) {
    if (this.state.active_step > index && this.validateSection(index)) {
      this.setState({active_step: index});
      window.location.href = '#'
    }
  }
  goToNextStep(e) {
    e.preventDefault();
    if (this.validateSection(this.state.active_step)) {
      this.setState({active_step : this.state.active_step + 1});
      window.location.href = '#'
    }    
  }
  onShipDateChange(new_date, cb) {
    this.setState({requested_ship_date: moment(new_date, 'MM-DD-YYYY').format('YYYY-MM-DD')});
    if (new_date.length === 10 && new_date.match(/(0[1-9]|1[012])[- \-.](0[1-9]|[12][0-9]|3[01])[- \-.](19|20)\d\d/)) {
      return cb({valid: true, message: ''});  
    }
    
    return cb({valid: false, message: 'Please format the date in the following format: MM-DD-YYYY'});    
  }
  onArrivalDateChange(new_date, cb) {
    this.setState({requested_arrival_date: moment(new_date, 'MM-DD-YYYY').format('YYYY-MM-DD')});
    if (new_date.length === 10 && new_date.match(/(0[1-9]|1[012])[- \-.](0[1-9]|[12][0-9]|3[01])[- \-.](19|20)\d\d/)) {
      return cb({valid: true, message: ''});  
    }
    
    return cb({valid: false, message: 'Please format the date in the following format: MM-DD-YYYY'});    
  }
  renderAccountNo() {
    if (this.state.freight_method !== 'pp') {
      return (
        <div>
          <label htmlFor="third_party_account_number">Collect/3rd Party Account Number</label>
          <input type='text' name='third_party_account_number' id='third_party_account_number' />
        </div>
      );
    }
  }
  renderBillingAddress() {
    if (this.state.billing_address) {
      return (
        <div>
          <Address name='Sold To' prefix='addresses[1]' addresses={this.props.addresses} type='BT' onChange={(address, valid) => {
            var addresses = this.state.addresses;
            addresses.billing_address = address;
            this.setState({billing_address_error: !valid});
          }} />
          <button className='small full error' onClick={() => {this.setState({billing_address_error: false, billing_address: false})}}><i className='fa fa-minus'></i> Billing Address</button>
        </div>
      );
    }
    return (
      <button className='small full' onClick={() => {this.setState({billing_address_error: true, billing_address: true})}}><i className='fa fa-plus'></i> Sold To Address</button>
    );
  }
  renderFreightAddress() {
    if (this.state.freight_address) {
      return (
        <div>
          <Address name='Freight' prefix='addresses[2]' addresses={this.props.addresses} type='FT' onChange={(address, valid) => {
            var addresses = this.state.addresses;
            addresses.freight_address = address;
            this.setState({freight_address_error: !valid});
          }} />
          <button className='small full error' onClick={() => {this.setState({freight_address_error: false, freight_address: false})}}><i className='fa fa-minus'></i> Freight Address</button>
        </div>
      );
    }
    return (
      <button className='small full' onClick={() => {this.setState({freight_address_error: true, freight_address: true})}}><i className='fa fa-plus'></i> Freight Address</button>
    );
  }
  renderDateError() {
    if (new Date(this.state.requested_arrival_date) - new Date(this.state.requested_ship_date) <= 0) {
      return (
        <div className="input-error">Arrival date must be after the Shipping date</div>
      );
    }
  }
  showError(name, message) {
    if (this.state[`${name}_error`]) {
      return <div className='input-error'>{message}</div>;
    }
  }
  render() {
    return (
      <form action='/orders/new' method='POST' autoComplete="off">
        <formgroup className={this.activeStep(1) ? 'active': ''} onClick={this.groupClicked.bind(this, 1)}>
            <h2 onClick={this.goToStep.bind(this, 1)}>
              Order Information
              <i className="fa fa-chevron-down"></i>
            </h2>
            <div className="form_inputs">
              <div className="input--half">
                <input type='hidden' name='customer_number' value={this.props.current_user.account_no} />
                <Input 
                  type='text' 
                  name='order_number' 
                  id='order_number' 
                  title='* Order Number'
                  onBlur={((input, cb) => {
                    this.setState({order_number: input});
               
                    if (input.trim() === '') {
                      this.setState({order_number_error: true});
                      return cb({valid: false, message: 'Order Number is required'});
                    }
                    
                    fetch(`/api/verify-unique-order?order_number=${input}&cust_num=${this.props.current_user.account_no}`)
                      .then(function(response) {
                        return response.json()
                      }).then((function(data) {
                   
                        if (!data.unique) {
                          this.setState({order_number_error: true});
                          return cb({valid: false, message: 'Order Number must be unique'})
                        }
                        this.setState({order_number_error: false});
                        return cb({valid: true});
                      }).bind(this));
                  })} />
                </div>
              <div className="input--half">
                <Input 
                  type='text' 
                  name='po_number' 
                  id='po_number' 
                  title='PO Number'
                  onBlur={(input, cb) => {
                    this.setState({po_number: input});
                    return cb({valid: true});
                  }}/>
              </div>              
              <label>Contact Name</label>
              <input type='text' className='disabled' name='name' readOnly='readOnly' value={this.props.current_user.name} />
              <label>Contact Email</label>
              <input type="text" className='disabled' name='email' readOnly='readOnly' value={this.props.current_user.email} />

              <div className="input--half">
                <Input 
                  type='text' 
                  name='' 
                  defaultValue={moment(this.state.requested_ship_date, 'YYYY-MM-DD').format('MM-DD-YYYY')}
                  title='* Requested Ship Date'
                  validate={this.onShipDateChange.bind(this)}
                  />  
                <input type="hidden" ref='ship_date' name='requested_ship_date' value={moment(this.state.requested_ship_date).format('YYYY-MM-DD')} />
              </div>              
              <div className="input--half">
                <Input 
                  type='text' 
                  name='' 
                  title='Requested Arrival Date'
                  validate={this.onArrivalDateChange.bind(this)}
                  />  
                <input type="hidden" ref='arrival_date' name='requested_arrival_date' value={this.state.requested_arrival_date === '' ? ' ' : moment(this.state.requested_arrival_date).format('YYYY-MM-DD')} />
              </div>
              {this.renderDateError()}
              <div className="input--half">
                <Select
                  name='carrier_code'
                  title='* Carrier Code'
                  options={this.state.carrier_codes}
                  className='input'
                  onChange={(value) => {
                    this.setState({scac_code: value});
                    if (value === '') return this.setState({scac_number_error: true});
                    return this.setState({scac_number_error: false});
                  }}
                  optionRenderer={(option, index) => {
                    return (
                      <option value={option.SCAC_CODE} key={index}>{option.NAME}</option>
                    )
                  }}
                />
              </div>
              <div className="input--half">
                <Select 
                  name='payment_method'
                  title='Freight Payment Method'
                  options={[['pp', 'PP - Prepaid'], ['co', 'CO - Collect'], ['tp', 'TP - Third Party']]}
                  className='input'
                  onChange={(value) => {
                    this.setState({freight_method: value});
                    this.forceUpdate.bind(this)
                  }}
                  />
              </div>
              {this.renderAccountNo()}
              <label htmlFor="notes">Comments</label>
              <textarea name="notes" id="notes" rows="10" onChange={(e) => {
                this.setState({notes: e.target.value});
              }}></textarea>
              <button onClick={this.goToNextStep.bind(this)} disabled={!this.validateSection(1)}>Continue</button>
            </div>
        </formgroup>
        <formgroup className={this.activeStep(2) ? 'active': ''}>
          <h2 onClick={this.goToStep.bind(this, 2)}>
            Shipping Information
            <i className="fa fa-chevron-down"></i>
          </h2>
          <div className="form_inputs">
            <Address prefix='addresses[0]' name='Shipping' addresses={this.props.addresses} type='ST' onChange={(address, valid) => {
              var addresses = this.state.addresses;
              addresses.shipping_address = address;
              this.setState({shipping_address_error: !valid});
            }} />
            {this.renderBillingAddress()}
            {this.renderFreightAddress()}
            <button onClick={this.goToNextStep.bind(this)} disabled={!this.validateSection(2)}>Continue</button>
          </div>
        </formgroup>
        <formgroup className={this.activeStep(3) ? 'active': ''}>
          <h2 onClick={this.goToStep.bind(this, 3)}>
            Add Items
            <i className="fa fa-chevron-down"></i>
          </h2>
          <div className="form_inputs">
            <OrderItems products={this.props.products} update={(items, valid) => {this.setState({order_items: items, order_items_valid: valid})}} />
            <button onClick={this.goToNextStep.bind(this)} disabled={!this.validateSection(3)}>Continue</button>
          </div>
        </formgroup>
        <formgroup className={this.activeStep(4) ? 'active' : ''}>
          <h2>
            Review
            <i className="fa fa-chevron-down"></i>
          </h2>
          <div className="form_inputs">
            <h3>Order Information</h3>
            <div className="review-item inner--half">
              <div className="review-item-label">Order Number</div>
              <div className="review-item-value">{this.state.order_number || ''}</div>
            </div>
            <div className="review-item inner--half">
              <div className="review-item-label">P.O. Number</div>
              <div className="review-item-value">{this.state.po_number}</div>
            </div>
            <div className="review-item">
              <div className="review-item-label">Contact Name</div>
              <div className="review-item-value">{this.props.current_user.name}</div>
            </div>
            <div className="review-item">
              <div className="review-item-label">Contact Email</div>
              <div className="review-item-value">{this.props.current_user.email}</div>
            </div>
            <div className="review-item inner--half">
              <div className="review-item-label">Requested Ship Date</div>
              <div className="review-item-value">{this.state.requested_ship_date}</div>
            </div>
            <div className="review-item inner--half">
              <div className="review-item-label">Requested Arrival Date</div>
              <div className="review-item-value">{this.state.requested_arrival_date.trim()}</div>
            </div>
            <div className="review-item inner--half">
              <div className="review-item-label">Carrier</div>
              <div className="review-item-value">{this.state.scac_code}</div>
            </div>
            <div className="review-item inner--half">
              <div className="review-item-label">Freight Payment Method</div>
              <div className="review-item-value">{(() => {
                switch (this.state.freight_method) {
                  case 'pp': return 'PP - Prepaid';
                  case 'tp': return 'TP - Third Party';
                  case 'co': return 'CO - Collect';
                }
              })()}</div>
            </div>
            <div className="review-item">
              <div className="review-item-label">Notes</div>
              <div className="review-item-value">{this.state.notes}</div>
            </div>
            {(() => {
              if (!this.state.addresses.shipping_address) return '';
              return (
                <div>
                  <h3>Shipping Address</h3>
                  <AddressReview address={this.state.addresses.shipping_address} />
                </div>
              );
            })()}
            {(() => {
              if (!this.state.addresses.billing_address) return '';
              return (
                <div>
                  <h3>Bill To Address</h3>
                  <AddressReview address={this.state.addresses.billing_address} />
                </div>
              );
            })()}
            {(() => {
              if (!this.state.addresses.freight_address) return '';
              return (
                <div>
                  <h3>Freight Address</h3>
                  <AddressReview address={this.state.addresses.freight_address} />
                </div>
              );
            })()}
            <h3>Order Items</h3>
            <OrderItemsReview items={this.state.order_items} />
            <input type="submit" value="Submit" />
          </div>
        </formgroup>
      </form>
    )
  }
}

module.exports = OrderForm;
