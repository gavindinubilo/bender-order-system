'use strict';
const React = require('react');
const BaseWrapper = require('../base_wrap.jsx');
require('../helpers/array_helpers.js');

class Inventory extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 0,
      per_page: 10,
      search: ''
    }
  }
  searchChange() {
    this.setState({search: this.refs.search.value, page: 0});
  }
  filterProducts() {
    const products = this.props.products.search(this.state.search);

    return products;
  }
  renderInventoryItems() {
    const temp_products = this.filterProducts();
    const temp_inventory = temp_products.slice((this.state.page * this.state.per_page), ((this.state.page * this.state.per_page) + this.state.per_page));

    const inventory = temp_inventory.map((product, index) => {
      return (
        <a href={`/inventory/${product.ITEM_ID}`} className='row-item'>
          <div className="attribute medium">
            {product.ITEM_ID}
          </div>
          <div className="attribute">
            {product.ITEM_DESCR_1}
          </div>
          <div className="attribute small">
            {product.ON_HAND_QTY}
          </div>        
          <div className="attribute small">
            {product.AVAIL_QTY}
          </div>
          <div className="attribute x-small">
            {(() => {
              if (product.LOTTED_FLG === 'N') return (
                <i className="fa fa-times"></i>
              );
              return (
                <i className="fa fa-check"></i>
              );
            })()}
          </div>
        </a>
      );
    });
    return inventory;
  }
  renderPrevButton() {
    if (this.state.page !== 0) {
      return (
        <a onClick={() => {this.setState({page: this.state.page - 1})}}>
          <i className="fa fa-chevron-left"></i>
          Prev
        </a>
      );
    }
  }
  renderNextButton() {
    const inventory = this.filterProducts();

    if (this.state.page < ((inventory.length) / this.state.per_page) - 1) {
      return (
        <a onClick={() => {this.setState({page: this.state.page + 1})}}>
          Next
          <i className="fa fa-chevron-right"></i>
        </a>
      );
    }
  }
  render() {
    return (
      <BaseWrapper>
        <div className="base-header">
          <h1>Inventory</h1>
          <a href={`/inventory/download`} download={true} className="btn base-header__button">
            <i className="fa fa-download"></i>
            Download
          </a>
        </div>
        <div>
          <div className='row-heading'>
            <div className="attribute medium">
              Product
            </div>
            <div className="attribute">
              Description
            </div>
            <div className="attribute small">
              On Hand
            </div>
            <div className="attribute small">
              Available
            </div>
            <div className="attribute x-small">Lotted</div>
          </div>
          <div className="row">
            <input type='text' ref='search' placeholder='Search Inventory' onChange={this.searchChange.bind(this)} />
          </div>
          {this.renderInventoryItems()}
          <div className="pagination">
            <div className="prev-wrapper">
              {this.renderPrevButton()}
            </div>
            <div className="next-wrapper">
              {this.renderNextButton()}
            </div>
          </div>
        </div>
      </BaseWrapper>
    );
  }
}

module.exports = Inventory;
