'use strict';
const React = require('react');
require('../helpers/array_helpers.js');

class Notifications extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 0,
      per_page: 10,
      notifications: props.notifications
    }
  }
  renderNotifications() {
    const notifications = this.state.notifications.map((notification) => {
      return (
        <a href={notification.link} className={`notification notification--${notification.type} notification--seen-${notification.seen}`}>{notification.text}</a>
      );
    });
    return notifications;
  }
  getNotifications(page) {
    this.setState({page: page});
    fetch(`/api/notifications?api_key=${this.props.current_user.api_key}&page=${page}`)
      .then(function(response) {
        return response.json();
      }).then(function(json) {
        this.setState({notifications: json.notifications});
      }.bind(this));
  }
  renderPrevButton() {
    if (this.state.page !== 0) {
      return (
        <a onClick={() => {this.getNotifications(this.state.page - 1)}}>
          <i className="fa fa-chevron-left"></i>
          Prev
        </a>
      );
    }
  }
  renderNextButton() {
    if (this.state.notifications.length >= this.state.per_page)  {
      return (
        <a onClick={() => {this.getNotifications(this.state.page + 1)}}>
          Next
          <i className="fa fa-chevron-right"></i>
        </a>
      );
    }
  }
  render() {
    return (
      <div>
        {this.renderNotifications()}
        <div className="pagination">
          <div className="prev-wrapper">
            {this.renderPrevButton()}
          </div>
          <div className="next-wrapper">
            {this.renderNextButton()}
          </div>
        </div>
      </div>
    );
  }
}

module.exports = Notifications;