'use strict';
const React = require('react');

class User extends React.Component {
  constructor(props) {
    super(props);
  }
  url(path) {
    return this.props.path_prefix + path;
  }
  render() {
    return (
      <a href={this.url(`/users/${this.props.user.username}`)}>
        <div className="user--list-item">
          <div className="user--list-item-user">
            <div className="user--list-item-user__name">
              {this.props.user.name}
            </div>          
            <div className="user--list-item-user__username">
              {this.props.user.username}
            </div>
            <div className="user--list-item-user__account_no">
              {this.props.user.account_no}
            </div>
          </div>        
          <div className="user--list-item-actions">
            <a href={this.url(`/users/${this.props.user.username}/edit`)} className="user--list-item-action">
              <i className="fa fa-pencil"></i>
            </a>
          </div>
        </div>
      </a>
    );
  }
}

module.exports = User;
