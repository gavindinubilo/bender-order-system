'use strict';
const React = require('react');

class Pagination extends React.Component {
  constructor(props) {
    super(props);
  }
  renderPrevButton() {
    if (this.props.page && this.props.page !== '0') {
      return (
        <a href={"?page=" + (parseInt(this.props.page) - 1)} data-page={this.props.page - 1}>
          <i className="fa fa-chevron-left"></i>
          Prev
        </a>
      );
    }
  }
  renderNextButton() {
    if (this.props.page <= this.props.page_count && this.props.page_count !== 0) {
      return (
        <a href={"?page=" + (parseInt(this.props.page) + 1)} data-page={this.props.page + 1}>
          Next
          <i className="fa fa-chevron-right"></i>
        </a>
      );
    }
  }
  render() {
    return (
      <div className="pagination">
        <div className="prev-wrapper">
          {this.renderPrevButton()}
        </div>
        <div className="next-wrapper">
          {this.renderNextButton()}
        </div>
      </div>
    )
  }
}

module.exports = Pagination;
