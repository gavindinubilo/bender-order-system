'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const ZeroState = require('../shared/helpers/zero_state.jsx');
const Pagination = require('../shared/pagination.jsx');
const moment = require('moment');

const status_labels = {
  'SCOM': 'Shipped',
  'OPEN': 'Open',
}

class Component extends React.Component {
  constructor(props) {
    super(props);
  }
  renderStatusLabel(order) {
    if (order.ORDER_TYPE === 'REGULAR' && order.ORDER_STATUS === 'SCOM') return 'Shipped';
    else return 'Open';
  }
  renderOrders() {
    if (!this.props.orders.length) {
      return (
        <ZeroState>
          {`No currently ${this.props.type.toLowerCase()} orders`} 
        </ZeroState>
      );
    }
    const orders = this.props.orders.map((order) => {
      return (
        <a href={`/orders/${order.ORDER_NO}`} className="row-item">
            <div className="attribute">{order.CUSTOMER_ORDER_NO}</div>
            <div className="attribute">{order.PO_NUMBER || '-'}</div>
            <div className="attribute">{order.SHIP_TO_ADDRESS || '-'}</div>
            <div className="attribute">{moment(order.ORDER_DATE).format('L')}</div>
            <div className="attribute">{order.SHIP_DATE ? moment(order.SHIP_DATE).format('L') : '-'}</div>
            <div className="attribute">{this.renderStatusLabel(order)}</div>
        </a>
      );
    });
    return orders;
  }
  renderDownloadButton() {
    if (this.props.orders.length === 0 || this.props.type.toLowerCase() === 'searched') return;
    return (
      <a href={`/orders/download?type=${this.props.type.toLowerCase()}`} title={this.props.type.toLowerCase() === 'open' ? '': `Will download the past 7 days of ${this.props.type.toLowerCase()} orders, contact your CLR for a custom report.`} download="true" className="btn base-header__button">
        <i className="fa fa-download"></i>
        Download
      </a>
    );
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper>
          <div className="base-header">
            <h1>{`${this.props.type} Orders`}</h1>
            {this.renderDownloadButton()}
          </div>                   
          <div className="row-heading">
            <div className="label">Order NO.</div>
            <div className="label">PO Number</div>
            <div className="label">Ship To</div>            
            <div className="label">Ordered</div>
            <div className="label">Shipped</div>
            <div className="label">Status</div>
          </div>
          {this.renderOrders()}
          <Pagination page={this.props.page || 0} page_count={this.props.orders.length < 16 ? -1 : 10000} />
        </BaseWrapper>
     </Layout>
   );
  }
}
module.exports = Component;
