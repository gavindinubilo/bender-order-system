'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const OrderForm = require('../shared/forms/order.jsx');

class New extends React.Component {
  constructor(props) {
    super(props);
  }
  getOrderData() {
    let order_data = this.props;
    order_data = JSON.stringify(order_data);
    order_data = encodeURI(order_data);
    return order_data;
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header='New Order'>
          <div id="react--order-form">
            <OrderForm {...this.props} />
          </div>
          <script dangerouslySetInnerHTML={{__html: `
          window.localStorage.setItem('order-form-data', "${this.getOrderData()}");
        `}}>
        </script>
        </BaseWrapper>
      </Layout>
    )
  }
}

module.exports = New;
