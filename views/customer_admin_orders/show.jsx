'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const ZeroState = require('../shared/helpers/zero_state.jsx');
const ProLink = require('../shared/helpers/pro_link.jsx');
const moment = require('moment');

const payment_methods = {
  'pp': 'PP - Prepaid',
  'co': 'CO - Collect',
  'tp': 'TP - Third Party'
};

class Show extends React.Component {
  constructor(props) {
    super(props);
  }
  renderItems() {
    if (!this.props.items || this.props.items.length === 0) {
      return (
        <ZeroState>
          No items in this order.
        </ZeroState>
      );
    } 
    
    const item_containers = this.props.items.map((item) => {
      return (
        <div className="row-item">
          <div className="attribute">{item.ITEM_ID}</div>
          <div className="attribute">{item.ITEM_DESCR_1}</div>
          <div className="attribute">{item.ORD_QTY}</div>
          <div className="attribute">{item.SHIP_QTY}</div>
        </div>
      )
    });
    return (
      <div>
        <div className="row-heading">
          <div className="attribute">Item ID</div>
          <div className="attribute">Description</div>
          <div className="attribute">Order Quantity</div>
          <div className="attribute">Ship Quantity</div>
        </div>
        {item_containers}
      </div>
    );
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper {...this.props}>
          <div className="base-header">
            <h1>Order {this.props.order.CUSTOMER_ORDER_NO}</h1>
          </div>
          <div className="row-heading">
            <div className="label">Item</div>
            <div className="attribute">Value</div>
          </div>
          <div className="row-item">
            <div className="label">Order Number</div>
            <div className="attribute">{this.props.order.CUSTOMER_ORDER_NO}</div>
          </div>
          <div className="row-item">
            <div className="label">Ship-To</div>
            <div className="attribute">{this.props.order.SHIPTO_NAME}</div>
          </div>
          <div className="row-item">
            <div className="label">Destination</div>
            <div className="attribute">{this.props.order.SHIP_TO_ADDRESS}</div>
          </div>
          <div className="row-item">
            <div className="label">P.O. Number</div>
            <div className="attribute">{this.props.order.PO_NUMBER}</div>
          </div>
          <div className="row-item">
            <div className="label">Pro/Tracking #</div>
            <div className="attribute"><ProLink pro_no={this.props.order.PRO_NO} scac={this.props.order.SCAC} /></div>
          </div>
          <div className="row-item">
            <div className="label">Payment Method</div>
            <div className="attribute">{payment_methods[this.props.order.FREIGHT_PAY_METHOD.toLowerCase()]}</div>
          </div>
          <div className="row-item">
            <div className="label">Carrier</div>
            <div className="attribute">{this.props.order.SCAC}</div>
          </div>
          <div className="row-item">
            <div className="label">Weight</div>
            <div className="attribute">{this.props.order.TOT_WGHT}</div>
          </div>
          <div className="row-item">
            <div className="label">Order Date</div>
            <div className="attribute">{moment(this.props.order.ORDER_DATE).format('L')}</div>
          </div>
          <div className="row-item">
            <div className="label">Req. Ship Date</div>
            <div className="attribute">{moment(this.props.order.REQ_SHIP_DATE).format('L')}</div>
          </div>
          <div className="row-item">
            <div className="label">Shipped Date</div>
            <div className="attribute">{this.props.order.SHIP_DATE ? moment(this.props.order.SHIP_DATE).format('L') : '-'}</div>
          </div>
          <div className="row-item">
            <div className="label">Comments</div>
            <div className="attribute">{this.props.order.COMMENT_LINE_ONE || '-'}</div>
          </div>
          <div className="base-header">
            <h1>Items</h1>
          </div>
          {this.renderItems()}
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = Show;