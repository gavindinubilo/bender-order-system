'use strict';
const React = require('react');
const Navigation = require('../shared/navigation.jsx');
const AdminNavigation = require('../shared/admin-navigation.jsx');
const CustomerNavigation = require('../shared/bottom-navigation.jsx');
const Footer = require('../shared/footer.jsx');
const Flashes = require('../shared/flashes.jsx');

class Application extends React.Component {
  constructor(props) {
    super(props);
  }
  renderJSScript() {
    if (process.env["API_ENV"] === 'production' || process.env["API_ENV"] === 'staging') {
      return (
        <script src="/js/application.js"></script>
      );
    }
    return (
      <div>
        <script src="http://localhost:2999/public/js/application.js"></script>
      </div>
    );
  }
  render() {
    return (
      <html>
        <head>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
          <link rel="stylesheet" type="text/css" href="/css/application.css" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link rel="shortcut icon" href="http://www.bendergroup.com/assets/favicon.png" />
          <meta name="description" />
          <meta charset="UTF-8" />

          <title>Bender Group</title>
        </head>
        <body>
          <div  id='js--ie-message' style={{display: 'none'}} className="notice-bar error">Internet Explorer produces errors on this website, for best results please use a modern browser (e.g. Google Chrome, Firefox)</div>
          <Navigation {...this.props} />
          <Flashes {...this.props} />
          {this.props.children}
          <Footer />
          <script dangerouslySetInnerHTML={{__html: `
            window.localStorage.setItem('current_user', '${JSON.stringify(this.props.current_user)}');
          `}}></script>
          {this.renderJSScript()}
        </body>
      </html>
    )
  }
}

module.exports = Application;
