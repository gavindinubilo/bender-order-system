'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const InventoryList = require('../shared/listings/inventory.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  getInventoryData() {
    let inventory_data = this.props;
    inventory_data = JSON.stringify(inventory_data);
    inventory_data = encodeURI(inventory_data);
    return inventory_data;
  }
  render() {
    return (
      <Layout {...this.props}>
        <div id="react--inventory-list">
          <InventoryList {...this.props} />
        </div>
        <script dangerouslySetInnerHTML={{__html: `
          window.localStorage.setItem('inventory-data', "${this.getInventoryData()}");
        `}}>
        </script>
      </Layout>
    );
  }
}

module.exports = Index;
