'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const ZeroState = require('../shared/helpers/zero_state.jsx');
const moment = require('moment');

class Show extends React.Component {
  constructor(props) {
    super(props);
  }
  renderLots() {
    if (this.props.product.LOTTED_FLG !== 'Y') return;
    if (this.props.lots.length === 0) return (
      <ZeroState>Item is lotted, but no lots are present</ZeroState>
    );

    var lots = this.props.lots.map((lot) => {
      return (
        <div className="row-item">
          <div className="attribute">{lot.LOT_NO}</div>
          <div className="attribute">{lot.AVAIL_QTY}</div>
          <div className="attribute">{lot.ON_HOLD_QTY}</div>
          <div className="attribute">{moment(lot.EXPIRATION_DATE).format('L')}</div>
        </div>
      );
    });
    return (
      <div>
        <h3>Per Lot</h3>
        <div className="row-heading">
          <div className="attribute">Lot NO.</div>
          <div className="attribute">Available</div>
          <div className="attribute">On Hold</div>
          <div className="attribute">Expiration Date</div>
        </div>
        {lots}
      </div> 
    );
  }
  render() {
    const product = this.props.product;
    return (
      <Layout {...this.props}>
        <BaseWrapper header={product.ITEM_ID}>
          <div className="row-heading">
            <div className="label">Item</div>
            <div className="attribute">Value</div>
          </div>
          <div className="row-item">
            <div className="label">Item ID</div>
            <div className="attribute">{product.ITEM_ID}</div>
          </div>
          <div className="row-item">
            <div className="label">Description</div>
            <div className="attribute">{product.ITEM_DESCR_1}</div>
          </div>
          <div className="row-item">
            <div className="label">On Hand Quantity</div>
            <div className="attribute">{product.ON_HAND_QTY}</div>
          </div>
          <div className="row-item">
            <div className="label">Available Quantity</div>
            <div className="attribute">{product.AVAIL_QTY}</div>
          </div>
          <div className="row-item">
            <div className="label">Damaged Quantity</div>
            <div className="attribute">{product.DMGD_QTY}</div>
          </div>
          <div className="row-item">
            <div className="label">Open Orders Quantity</div>
            <div className="attribute">{product.OPEN_ORDS_QTY}</div>
          </div>
          {this.renderLots()}
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = Show;