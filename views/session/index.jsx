'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const Flashes = require('../shared/flashes.jsx');
const Footer = require('../shared/footer.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <html>
        <head>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
          <link rel="stylesheet" type="text/css" href="/css/application.css" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <meta name="description" />
          <meta charset="UTF-8" />
          <title>Bender Group</title>
        </head>
        <body>
          <div className="session-wrapper">
            <div className="session">
              <div className="session-logo">
                <img src="/images/logo.png" alt=""/>
              </div>
              <Flashes {...this.props} />
              <div className="session-inner">
                <div className="session-header"><h1>Login</h1></div>
                <form action="/login/auth" method="POST">
                  <label htmlFor="username">Username</label>
                  <input type="text" name="username" id="username"/>
                  <label htmlFor="password">Password</label>
                  <input type="password" name="password" id="password"/>
                  <input type="submit" value='Login' />
                </form>
                <p>
                  Customers of Bender Warehouse can access our database for their orders and shipments information, 24 hours a day, 7 days a week.
                </p>
                <p>
                  Bender Group authorizes our customer to use this site for the sole purpose of tracking orders and inventories. Any other use of this system and information is strictly prohibited.
                </p>
              </div>
            </div>
          </div>
          <Footer />
        </body>
      </html>
    )
  }
}

module.exports = Index;
