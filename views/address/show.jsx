'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');

class Show extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper>
          <div className="base-header">
            <h1>Address</h1>
            <a href={`/addresses/${this.props.address._id}/edit`} className="btn base-header__button">
              <i className="fa fa-pencil"></i>
              Edit
            </a>
          </div>  
          <div className="row-heading">
            <div className="label">Name</div>
            <div className="label">Value</div>
          </div>
          <div className="row-item">
            <div className="label">Recipient Name</div>
            <div className="label">{this.props.address.name}</div>
          </div>
          <div className="row-item">
            <div className="label">E-Mail</div>
            <div className="label">{this.props.address.email || '-'}</div>
          </div>
          <div className="row-item">
            <div className="label">Phone Number</div>
            <div className="label">{this.props.address.phone || '-'}</div>
          </div>
          <div className="row-item">
            <div className="label">Line One</div>
            <div className="label">{this.props.address.line_one}</div>
          </div>
          <div className="row-item">
            <div className="label">Line Two</div>
            <div className="label">{this.props.address.line_two || '-'}</div>
          </div>
          <div className="row-item">
            <div className="label">Line Three</div>
            <div className="label">{this.props.address.line_three || '-'}</div>
          </div>
          <div className="row-item">
            <div className="label">ZIP Code</div>
            <div className="label">{this.props.address.zip_code}</div>
          </div>
          <div className="row-item">
            <div className="label">City</div>
            <div className="label">{this.props.address.city}</div>
          </div>
          <div className="row-item">
            <div className="label">State</div>
            <div className="label">{this.props.address.state}</div>
          </div>
          <div className="row-item">
            <div className="label">Country</div>
            <div className="label">{this.props.address.country}</div>
          </div>
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = Show;