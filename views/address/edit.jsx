'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const Input = require('../shared/helpers/Input.jsx');
const _ = require('underscore');

const AddressForm = require('../shared/forms/address.jsx');

class Edit extends React.Component {
  constructor(props) {
    super(props);
  }
  getAddressData() {
    return encodeURI(JSON.stringify(this.props.address));
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header='Edit Address'>
          <form action="/addresses/edit" method="POST">
            <input type="hidden" name="_id" value={this.props.address._id}/>
            <div id="react--address-form">
              <AddressForm {...this.props.address} />
            </div>
            <script dangerouslySetInnerHTML={{__html: `
              window.localStorage.setItem('address-data', "${this.getAddressData()}");
            `}}>
            </script>
          </form>
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = Edit;