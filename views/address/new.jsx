'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const NotificationList = require('../shared/listings/notification.jsx');
const Input = require('../shared/helpers/Input.jsx');

const AddressForm = require('../shared/forms/address.jsx');

class New extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header='New Address'>
          <form action="/addresses/new" method="POST">
            <div id="react--address-form">
              <AddressForm />
            </div>
          </form>
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = New;