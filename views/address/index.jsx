'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const NotificationList = require('../shared/listings/notification.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  renderAddresses() {
    const addresses = this.props.addresses.map((address) => {
      return (
        <a href={`/addresses/${address._id}`} className="row-item">
          <div className="label">{address.name}</div>
          <div className="label">{address.line_one}</div>
          <div className="label">{address.city}</div>
          <div className="label small">{address.state}</div>
          <div className="label medium">{address.country}</div>
          <div className="label medium">{address.zip_code}</div>
        </a>
      );
    });
    return addresses;
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper>
          <div className="base-header">
            <h1>Addresses</h1>
            <a href="/addresses/new" className="btn base-header__button">
              <i className="fa fa-plus"></i>
              New
            </a>
          </div>  
          <div className="row-heading">
            <div className="label">Name</div>
            <div className="label">Line One</div>
            <div className="label">City</div>
            <div className="label small">State</div>
            <div className="label medium">Country</div>
            <div className="label medium">ZIP Code</div>
          </div>
          {this.renderAddresses()}
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = Index;