'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const User = require('../shared/models/user.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  renderUsers() {
    const users = this.props.users.map((user) => {
      return <User user={user} path_prefix='/admin' />
    });
    return users;
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper>
          <div className="base-header">
            <h1>Users</h1>
            <a href="/admin/users/new" className="btn base-header__button">
              <i className="fa fa-plus"></i>
              New
            </a>
          </div>
          {this.renderUsers()}
        </BaseWrapper>
      </Layout>
    )
  }
}

module.exports = Index;
