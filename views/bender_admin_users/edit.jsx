'use strict';
const React = require('react');
const Layout = require('../layouts/application');
const BaseWrapper = require('../shared/base_wrap');
const UserForm = require('../shared/forms/user.jsx');

class Edit extends React.Component {
  constructor(props) {
    super(props);
  }
  getUserData() {
    var user_props = this.props.user;
    return user_props;
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header={`Edit ${this.props.user.username}`}>
          <div id='react--user-form'>
            <UserForm {...this.props.user} />
          </div>
        </BaseWrapper>
        <script dangerouslySetInnerHTML={{__html: `
          window.localStorage.setItem('user-form-data', '${JSON.stringify(this.getUserData())}');
        `}}>
        </script>
      </Layout>
    )
  }
}

module.exports = Edit;
