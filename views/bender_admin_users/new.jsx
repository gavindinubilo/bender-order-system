'use strict';
const React = require('react');
const Layout = require('../layouts/application');
const BaseWrapper = require('../shared/base_wrap');
const UserForm = require('../shared/forms/user.jsx');

class Edit extends React.Component {
  constructor(props) {
    super(props);
  }
  getUserData() {
    var user_props = this.props.user;
    return user_props;
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header={'New User'}>
          <div id='react--user-form'>
            <UserForm />
          </div>
        </BaseWrapper>
      </Layout>
    )
  }
}

module.exports = Edit;
