'use strict';
const React = require('react');
const Layout = require('../layouts/application');
const BaseWrapper = require('../shared/base_wrap');
const moment = require('moment');

class Show extends React.Component {
  constructor(props) {
    super(props);
  }
  renderEditButton() {
    if (this.props.current_user.bender_admin) {
      return (
        <a href={`/admin/users/${this.props.user.username}/edit`} className="btn base-header__button">
          <i className="fa fa-pencil"></i>
          Edit
        </a>
      );
    }
  }
  renderResetPWButton() {
    if (this.props.current_user.bender_admin) {
      return (
        <a href={`/reset-password/${this.props.user.username}`} className="btn base-header__button">
          <i className="fa fa-refresh"></i>
          Reset Password
        </a>
      );
    } 
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper>
          <div className="base-header">
            <h1>{this.props.user.name}</h1>
            {this.renderEditButton()}
            {this.renderResetPWButton()}
          </div>
          <div className="row-heading">
            <div className="label">Label</div>
            <div className="attribute">Attribute</div>
          </div>
          <div className="row-item">
            <div className="label">Name</div>
            <div className="attribute">{this.props.user.name}</div>
          </div>
          <div className="row-item">
            <div className="label">Username</div>
            <div className="attribute">{this.props.user.username}</div>
          </div>
          <div className="row-item">
            <div className="label">Email</div>
            <div className="attribute">{this.props.user.email}</div>
          </div>
          <div className="row-item">
            <div className="label">Admin</div>
            <div className="attribute">{this.props.user.customer_admin.toString()}</div>
          </div>
          <div className="row-item">
            <div className="label">Created On</div>
            <div className="attribute">{moment(this.props.user.created_at).calendar()}</div>
          </div>
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = Show;