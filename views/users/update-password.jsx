'use strict';
const React = require('react');
const Layout = require('../layouts/application');
const BaseWrapper = require('../shared/base_wrap');

class UpdatePassword extends React.Component {
  constructor(props) {
    super(props);
  }
  getUserData() {
    var user_props = this.props.user;
    return user_props;
  }
  render() {
    return (
      <Layout {...this.props}>
        <div className="session-wrapper">
          <div className="session">
            <div className="session-inner">
              <div className="session-header">
                <h1>Update Password</h1>
              </div>
              <form method='POST' action='/update-password'>
                <label htmlFor='password'>Password</label>
                <input type='password' name='password' id='password' />
                <input type="submit" value='Update Password' />
              </form>
            </div>
          </div>
        </div>        
      </Layout>
    );
  }
}

module.exports = UpdatePassword;