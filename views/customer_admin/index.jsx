'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const NotificationList = require('../shared/listings/notification.jsx');
const OrderSearch = require('../shared/forms/search.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props)
  }
  getSearchData() {
    let search_data = {};
    search_data.carrier_codes = this.props.carrier_codes;
    search_data.carrier_codes.unshift({'SCAC_CODE': '', 'NAME': 'Choose One'});
    return JSON.stringify(search_data);
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header='Order Search'>
          <div id="react--order-search">
            <OrderSearch />
          </div>
          <script dangerouslySetInnerHTML={{__html: `
            window.localStorage.setItem('search-data', '${this.getSearchData()}');
          `}}>
          </script>
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = Index;
