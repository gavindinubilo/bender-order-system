'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const NotificationList = require('../shared/listings/notification.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  getNotificationData() {
    let notification_data = this.props;
    notification_data = JSON.stringify(notification_data);
    notification_data = encodeURI(notification_data);
    return notification_data;
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header="Admin Notifications">
          <p>The notifications below are just for testing, they will be changed once we launch the new site.</p>
          <div id="react--notification-list">
            <NotificationList {...this.props} />
          </div>
          <script dangerouslySetInnerHTML={{__html: `
            window.localStorage.setItem('notification-data', "${this.getNotificationData()}");
          `}}>
          </script>
        </BaseWrapper>
      </Layout>
    );
  }
}

module.exports = Index;
