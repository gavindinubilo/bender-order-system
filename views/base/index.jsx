'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header="Hello World">
          {this.props.current_user.username}
        </BaseWrapper>              
      </Layout>
    )
  }
}

module.exports = Index;
