'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header="Something Went Wrong">
          <p>{"Something happened on our end, please try again, if the problem persists contact a Bender Group Admin."}</p>
          <p>
            <button className='btn js--goback'>Go Back</button>
          </p>
        </BaseWrapper>
      </Layout>
    )
  }
}

module.exports = Index;
