'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper header="Page Not Found">
          <p>We couldn't find this page. If you think this is an error please contact a Bender Group Admin.</p>
        </BaseWrapper>
      </Layout>
    )
  }
}

module.exports = Index;
