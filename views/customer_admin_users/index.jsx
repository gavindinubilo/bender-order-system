'use strict';
const React = require('react');
const Layout = require('../layouts/application.jsx');
const BaseWrapper = require('../shared/base_wrap.jsx');
const User = require('../shared/models/user.jsx');
const Pagination = require('../shared/pagination.jsx');

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  renderUsers() {
    const users = this.props.users.map((user) => {
      return <User user={user} path_prefix='' />
    });
    return users;
  }
  renderNewButton() {
    if (this.props.current_user.customer_admin) {
      return (
        <a href="/users/new" className="btn base-header__button">
          <i className="fa fa-plus"></i>
          New
        </a>
      );
    }
  }
  render() {
    return (
      <Layout {...this.props}>
        <BaseWrapper>
          <div className="base-header">
            <h1>Users</h1>
            {this.renderNewButton()}
          </div>
          {this.renderUsers()}
          <Pagination {...this.props} page_count={this.props.users.length < 16 ? -1 : 10000} />
        </BaseWrapper>
      </Layout>
    )
  }
}

module.exports = Index;
