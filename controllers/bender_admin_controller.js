'use strict';
const Notification = require('../models').Notification;

module.exports = {
  index: function(req, res) {
    Notification.find({}).sort({'created_at': 'desc'}).limit(10).exec((err, notifications) => {
      res.view('bender_admin/index', {notifications: JSON.parse(JSON.stringify(notifications))});
    });
  }
}