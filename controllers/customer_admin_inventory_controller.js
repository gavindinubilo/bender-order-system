'use strict';

const test_product_data = require('../tmp/temp_product_data');

const m = require('../models');
const User = m.User;
const Inventory = m.Oracle.Inventory;
const Lot = m.Oracle.Lot;

const async = require('async');

module.exports = {
  index: function(req, res) {
    Inventory.getProductsForUser(req.yar.get('user').account_no, (err, products) => {
      return res.view('customer_admin_inventory/index', {products: products});
    });
  },
  show: function(req, res) {
    let lots = false;

    async.parallel({
      product: (callback) => {
        Inventory.getProduct(req.yar.get('user').account_no, req.params.item_id, (err, product) => callback(err, product));
      },
      lots: (callback) => {
        Lot.getLotsForItem(req.yar.get('user').account_no, req.params.item_id, (err, lots) => callback(err, lots));
      }
    }, (err, results) => {
      console.log(results.product, results.lots);
      return res.view('customer_admin_inventory/show', {product: results.product[0], lots: results.lots})
    });
  }
}