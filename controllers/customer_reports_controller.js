'use strict';
const m = require('../models');
const User = m.User;
const Order = m.Oracle.Order;
const Inventory = m.Oracle.Inventory;

const report = require('../services/build_report.js');

const order_methods = {
  open: Order.getItemsAndOpenOrders,
  shipped: Order.getItemsAndShippedOrders,
  searched: 'ok'
}

module.exports = {
  orders: function (req, res) {
    if (!req.query.type || !order_methods[req.query.type]) {
      return res('need to provide a valid type');
    }
      
    return order_methods[req.query.type](req.yar.get('user').account_no, (err, orders) => {
      return report.order(orders, (csv) => {
        res(csv).header('Content-Type', 'text/csv').header('Content-Disposition', 'attachment; filename=orders.csv');
      });
    });
  },
  inventory: function (req, res) {
    Inventory.getProductsForUser(req.yar.get('user').account_no, (err, items) => {
      return report.inventory(items, (csv) => {
        res(csv).header('Content-Type', 'text/csv').header('Content-Disposition', 'attachment; filename=inventory.csv');
      });
    });
  }
}