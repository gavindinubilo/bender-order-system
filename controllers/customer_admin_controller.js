'use strict';

const m = require('../models');
const User = m.User;
const Notification = m.Notification;
const better_error = require('../services/build_error').build;
const Boom = require('boom');

module.exports = {
  index: function(req, res) {
    res.view('customer_admin/index');
  }
};
