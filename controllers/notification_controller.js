'use strict';

const m = require('../models');
const User = m.User;
const Notification = m.Notification;

module.exports = {
  index: function(req, res) {
    res('index');
  },
  show: function(req, res) {
    res('show');
  },
  list: function(req, res) {    
    User.findOne({api_key: req.query.api_key}).exec(function(err, user) {
      if (err) res(err);

      let search_obj = {};
      if (!user.bender_admin) search_obj = {account_no: user.account_no};

      Notification.find(search_obj).sort({'created_at': 'desc'}).limit(10).skip(req.query.page * 10).exec(function(err, notifications) {
        res({notifications: JSON.parse(JSON.stringify(notifications))});
        if (user.bender_admin) return;

        for (let notification of notifications) {
          notification.seen = true;
          notification.save((err) => {
            if (err) console.log(err);
          });
        }  
      });
    });
  }
}
