'use strict';

const m = require('../models');
const User = m.User;
const Notification = m.Notification;
const better_error = require('../services/build_error').build;
const Boom = require('boom');
const WelcomeMailer = require('../mailer/welcome_mailer');

module.exports = {
  index: function(req, res) {
    let per_page = 16
    let offset_amt = per_page * parseInt(req.query.page);
    User.find({}).sort('account_no').skip(offset_amt).limit(per_page).exec(function(err, users) {
      if (err) return res(err);
      User.count({}, function(err, count) {
        res.view('bender_admin_users/index', {users: users, page: req.query.page, page_count: Math.floor(count / per_page)});
      });
    });
  },
  search: function(req, res) {
    User.find()
  },
  show: function(req, res) {
    User.findOne({username: req.params.username}, function(err, user) {
      if (!user) return res.view('errors/404').code(404);

      res.view('bender_admin_users/show', {user: user});
    });
  },
  new: function(req, res) {
    res.view('bender_admin_users/new');
  },
  create: function(req, res) {
    let new_user = new User(req.payload);
    let temp_pw = new_user.genTempPW();
    new_user.save(function(err, user) {
      if (err) {
        req.yar.flash('error', better_error(err));
        return res.redirect('/admin/users/new');
      }
      let m = new WelcomeMailer();
      m.send(new_user);
      req.yar.flash('notice', 'You successfully made this user');

      new Notification({
        account_no: new_user.account_no,
        type: 'notice',
        text: `${new_user.name} was created.`,
        link: `#`
      }).save((err) => {console.log(err)});

      return res.redirect(`/admin/users/${user.username}`);
    });
  },
  edit: function(req, res) {
    User.findOne({username: req.params.username}, function(err, user) {
      if (!user) return res(Boom.notFound('User not found'));
      res.view('bender_admin_users/edit', {user: user});
    });
  },
  update: function(req, res) {
    req.payload.bender_admin = req.payload.bender_admin || false;
    req.payload.customer_admin = req.payload.customer_admin || false;
    User.findOneAndUpdate({username: req.params.username}, req.payload, function(err, user) {
      if (err) {
        req.yar.flash('error', `Could not find user.`);
        return res.redirect(`/admin/users`)
      }

      req.yar.flash('notice', 'Successfully updated this user.');
      return res.redirect(`/admin/users/${req.payload.username}`);
    });
  },
  delete: function(req, res) {
    User.findOne({username: req.params.username}, function(err, user) {
      if (!user || err) {
        req.yar.flash('error', `Could not find user with username: '${req.params.username}'.`);
        return res.redirect('/admin/users')
      }

      user.remove(function(err) {
        if (!err) {
          req.yar.flash('notice', 'You successfully deleted a user.');
          return res.redirect('/admin/users');
        }
      })
    });
  }
}
