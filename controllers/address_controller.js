'use strict';
const m = require('../models');
const Address = m.Address;

module.exports = {
  index: function(req, res) {
    Address.find({account_no: req.yar.get('user').account_no}).exec(function(err, addresses) {
      res.view('address/index', {addresses, addresses});
    });
  },
  show: function(req, res) {
    Address.findOne({_id: req.params.id}).exec(function(err, address) {
      res.view('address/show', {address: address});
    });
  },
  edit: function(req, res) {
    Address.findOne({_id: req.params.id}).exec(function(err, address) {
      res.view('address/edit', {address: address});
    });
  },
  new: function(req, res) {
    res.view('address/new');
  },
  update: function(req, res) {
    Address.findOneAndUpdate({_id: req.payload._id}, req.payload).exec(function(err, address) {
      res.redirect(`/addresses/${address._id}`);
    });
  },
  create: function(req, res) {
    const address = new Address(req.payload);
    address.account_no = req.yar.get('user').account_no;
    address.save(function(err, new_address) {
      if (err) {
        req.yar.flash('error', 'There was an error saving this address');
        return res.redirect('/addresses/new');
      } 
      req.yar.flash('notice', 'Address successfully created');
      return res.redirect(`/addresses/${new_address._id}`);
    });
  },
  delete: function(req, res) {
    Address.findOne({_id: req.query._id}).exec(function(err, address) {
      if (err) {
        req.yar.flash('error', 'We couldn\'t find this address');
        return res.redirect('/addresses');
      }
      address.remove(function(err) {
        if (err) {
          req.yar.flash('error', 'There was an issue deleting the address');
        } else {
          req.yar.flash('notice', 'We successfully deleted the address');
        }
        return res.redirect('/addresses');
      });
    });
  }
}