'use strict';
const m = require('../models');
const User = m.User;
const Oracle = m.Oracle;
const Base = Oracle.Base;
const Order = Oracle.Order;

module.exports = {
  verify_unique: function(req, res) {
    if (!req.query.type && !req.query.value) return res.view('errors/500').code(500);
    const check = {};
    check[req.query.type] = req.query.value;
    User.findOne(check, function(err, user) {
      if (user) return res('no');
      res('ok');
    });
  },
  verify_unique_order: function(req, res) {
    Order.is_unique_order_number(req.query.order_number, req.query.cust_num, (err, order) => {
      return res({order_number: req.query.order_number, unique: (order[0]['COUNT(*)'] === 0)});
    });
  },
  query: function(req, res) {
    Base.run(decodeURI(req.payload.query), (err, result) => {
      return res(result);
    });    
  }
}
