'use strict';
const test_order_data = require('../tmp/temp_order_data');
const test_product_data = require('../tmp/temp_product_data');
const m = require('../models');
const async = require('async');
const User = m.User;
const Order = m.Oracle.Order;
const Inventory = m.Oracle.Inventory;
const Carrier = m.Oracle.Carrier;
const Address = m.Address;
const Boom = require('boom');
const _ = require('underscore');
const qs = require('querystring');

const js2xmlparser = require('js2xmlparser');
const uploader = require('../services/uploader');
const OrderValidator = require('../validators/Order_Validator.js');
let DBValidator = null;

if (process.env['API_ENV'] === 'production') {
  DBValidator = require('../validators/Db_Validator.js');
}

module.exports = {
  index: function(req, res) {
    const page = (req.query.page) ? parseInt(req.query.page) + 1 : 1;
    return Order.getOrdersForUser(req.yar.get('user').account_no, page, (err, orders) => {
      return res.view('customer_admin_orders/index', {orders: orders, page: req.query.page, type: 'Open'});
    });
  },
  shipped: function(req, res) {    
    const page = (req.query.page) ? parseInt(req.query.page) + 1 : 1;
    return Order.getShippedOrdersForUser(req.yar.get('user').account_no, page, (err, orders) => {
      return res.view('customer_admin_orders/index', {orders: orders, page: req.query.page, type: 'Shipped'});
    });
  },
  new: function(req, res) {
    if (!req.yar.get('user').customer_admin) {
      req.yar.flash('notice', 'You don\'t have access to that page');
      return res.redirect('/');
    }
    async.parallel({
      products: (callback) => {
        const products = Inventory.getProductsForUser(req.yar.get('user').account_no, (err, products) => callback(null, products));
      },
      carrier_codes: (callback) => {
        return Carrier.getCodes((err, codes) => callback(null, codes));
      },
      addresses: (callback) => {
        Address.find({account_no: req.yar.get('user').account_no}).exec(function(err, addresses) {
          callback(null, addresses);
        });
      }
    }, (err, results) => {
      return res.view('customer_admin_orders/new', {products: results.products, carrier_codes: results.carrier_codes, addresses: results.addresses});  
    });
  },
  create: function(req, res) {
    if (!req.yar.get('user').customer_admin) {
      req.yar.flash('notice', 'You don\'t have access to that page');
      return res.redirect('/');
    }
    async.parallel([
      function(callback) {
        console.log(req.payload);
        OrderValidator.validate(req.payload, function(err) {
          if (err) return callback(err, null);
          callback(null, req.payload);
        });
      },
      function(callback) {
        if (process.env['API_ENV'] !== 'production') return callback(null, req.payload);
        if (!DBValidator.validate_cust_number(req.payload.customer_number)) {
         return callback('Not a valid customer number', null);
        };
        return callback(null, req.payload);
      },
      function(callback) {
        if (process.env['API_ENV'] !== 'production') return callback(null, req.payload);
        if (!DBValidator.validate_order_number(req.payload.order_number, req.payload.customer_number)) {
         return callback('Not a valid order number', null);
        };
        return callback(null, req.payload);
      },
      function(callback) {
        if (process.env['API_ENV'] !== 'production') return callback(null, req.payload);
        async.each(req.payload.line_items, function(item, cb) {
          if (!DBValidator.validate_bender_item_id(item.bender_item_id, req.payload.customer_number)) {
            return cb('Not a valid item id', null);
          };
          return cb(null, req.payload);
        }, function(err) {
          callback(err, req.payload);
        });
      }
    ], function(err, results) {
      if (err) {
        console.log(err);
        return res.view('errors/500').code(500);
      }

      var xml = js2xmlparser('order', req.payload);
      uploader(xml, req.payload.customer_number);

      req.yar.flash('notice', 'Order successfully placed');
      return res.redirect('/');
    });
  },
  show: function(req, res) {
    async.parallel({
      order: (callback) => {
        Order.getOrderForUser(req.yar.get('user').account_no, req.params.order, (err, order) => callback(err, order));
      },
      items: (callback) => {
        Inventory.getItemsForOrder(req.params.order, (err, items) => callback(err, items));
      }
    }, (err, results) => {
      if (!results.order[0]) {
        return res.view('errors/404').code(404);
      }
      return res.view('customer_admin_orders/show', {order: results.order[0], items: results.items});
    })
  },
  search: function(req, res) {
    Order.searchOrdersForUser(req.yar.get('user').account_no, req.query, req.query.page, (err, orders) => {
      return res.view('customer_admin_orders/index', {orders: orders, page: req.query.page, type: 'Searched', query: qs.stringify(req.query)});
    });
  }
}
