// All controllers are written in here, only place
//  you will reference these are in routes.js
//  Make controller name capitalized to separate words
//  Controller file name should have controller name
//  then _controller (e.g. base_controller) and should
//  be all lowercase

module.exports = {
  Base: require('./base_controller'), // Base controller, mainly homepage
  Session: require('./session_controller'), // Session controller, for setting/destroying session
  Users: require('./users_controller'), // Users controller, for anything involving users
  BenderAdmin: require('./bender_admin_controller'), // Bender Admin controller, for Bender Admins only
  BenderAdminUsers: require('./bender_admin_users_controller'), // Bender Admin Users controller, for Bender Admins only
  CustomerAdmin: require('./customer_admin_controller'), // Base Controller for Customer Admins
  CustomerAdminUsers: require('./customer_admin_users_controller'), // Users Controller for Customer Admins
  CustomerAdminOrders: require('./customer_admin_orders_controller'), // Orders Controller for Customer Admins
  CustomerAdminInventory: require('./customer_admin_inventory_controller'), // Orders Controller for Customer Admins
  CustomerReports: require('./customer_reports_controller'), // Required for building CSV reports
  Api: require('./api_controller'), // API Endpoint
  Notification: require('./notification_controller'), // Notification Endpoint
  Address: require('./address_controller'), // Address Controller
}
