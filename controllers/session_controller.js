var m = require('../models');
var User = m.User;

module.exports = {
  index: function(req, res) {
    res.view('session/index');
  },
  // Logout handler
  destroy: function(req, res) {
    req.yar.reset();
    req.yar.flash('notice', 'You\'ve been logged out');
    res.redirect('/login');
  },
  // /login/auth has username and password in payload
  auth: function(req, res) {
    return User.findOne({'username': req.payload.username}, function(err, user) {
      if (err) return res.redirect('/login');
      if (user && user.authenticate(req.payload.password)) {
        req.yar.set('user', user);
        req.yar.flash('notice', 'You\'ve been successfully signed in.');
        return res.redirect('/');
      }

      req.yar.flash('error', 'Your username or password was incorrect. If you are having difficulties logging in, please speak with a Bender Administrator to get your password reset.');
      return res.redirect('/login');
    });
  }
}
