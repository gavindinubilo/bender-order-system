const js2xmlparser = require('js2xmlparser');
const Joi = require('joi');
const async = require('async');
const Boom = require('boom');
const m = require('../models');
const Carrier = m.Oracle.Carrier;

if (process.env['API_ENV'] === 'production') {
  const OrderValidator = require('../validators/Order_Validator.js');
  const DBValidator = require('../validators/Db_Validator.js');
}

const uploader = require('../services/uploader');

module.exports = {
  index: function(req, res) {
    Carrier.getCodes((err, codes) => {
      return res.view('customer_admin/index', {carrier_codes: codes});
    });
  },
  // This monster is what validates orders, it works by using the async module
  api_index: function(req, res) {
    async.parallel([
      function(callback) {
        OrderValidator.validate(req.payload, function(err) {
          if (err) return callback(err, null);
          callback(null, req.payload);
        });
      },
      function(callback) {
        if (!DBValidator.validate_cust_number(req.payload.customer_number)) {
         return callback('Not a valid customer number', null);
        };
        return callback(null, req.payload);
      },
      function(callback) {
        if (!DBValidator.validate_order_number(req.payload.order_number, req.payload.customer_number)) {
         return callback('Not a valid order number', null);
        };
        return callback(null, req.payload);
      },
      function(callback) {
        async.each(req.payload.line_items, function(item, cb) {
          if (!DBValidator.validate_bender_item_id(item.bender_item_id, req.payload.customer_number)) {
            return cb('Not a valid item id', null);
          };
          return cb(null, req.payload);
        }, function(err) {
          callback(err, req.payload);
        });
      }
    ], function(err, results) {
      if (err) return res(Boom.badRequest(err));

      const xml = js2xmlparser('order', req.payload);
      uploader(xml, req.payload.customer_number);

      res({
          'statusCode': 200,
          'message': 'order successfully placed'
      });
    });
  }
}
