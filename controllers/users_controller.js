const m = require('../models');
const User = m.User;

module.exports = {
  index: function(req, res) {
    User.find({}, function(err, users) {
      if (err) return res.view('errors/500').code(500);
      res('users/index', {users: users});
    });
  },
  change_password: function(req, res) {
    res.view('users/update-password');
  },
  update_password: function(req, res) {
    const current_user = req.yar.get('user');
    User.findOne({_id: current_user._id}, function(err, user) {
      user.updatePassword(req.payload.password, function(err, user) {
        if (err) {
          req.yar.flash('error', 'There was an issue processing your request, please try again.') 
          return res.redirect('/update-password');
        }
        req.yar.flash('notice', 'Your password has been successfully updated');
        return res.redirect('/');
      });
    });
  },
  reset_password: function(req, res) {
    User.findOne({username: req.params.username}).exec(function(err, user) {
      if (!user) return res.redirect('/');

      user.genLoginKey();

      user.save(function() {
        user.sendPasswordResetEmail();

        req.yar.flash('notice', 'This user has received an email containing a link to reset their password.');
        return res.redirect(`/admin/users/${req.params.username}`);
      });
    });
  },
  verify: function(req, res) {
    User.findOne({username: req.query.user, login_key: req.query.key, can_login_from_key: true}).exec(function(err, user) {
      if (!user) {
        req.yar.flash('error', 'You don\'t have access to that page.');
        return res.redirect('/');
      }
      req.yar.flash('notice', 'Welcome, to get started please update your password.');
      user.can_login_from_key = false;
      req.yar.set('user', user);
      user.save(function(err) {
        return res.view('users/update-password');  
      });
    });
  }
}