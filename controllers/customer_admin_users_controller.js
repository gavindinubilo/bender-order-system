'use strict';

const m = require('../models');
const User = m.User;
const Notification = m.Notification;
const better_error = require('../services/build_error').build;
const Boom = require('boom');
const WelcomeMailer = require('../mailer/welcome_mailer');

module.exports = {
  index: function(req, res) {
    let current_user  = req.yar.get('user');
    let per_page = 4
    let offset_amt = per_page * parseInt(req.query.page);
    User.find({account_no: current_user.account_no}).sort('name').skip(offset_amt).limit(per_page).exec(function(err, users) {
      if (err) return res(err);
      User.count({}, function(err, count) {
        res.view('customer_admin_users/index', {users: users, page: req.query.page || 0, page_count: Math.floor(count / per_page)});
      });
    });
  },
  show: function(req, res) {
    User.findOne({username: req.params.username}, function(err, user) {
      if (!user) return res.view('errors/404').code(404);

      res.view('customer_admin_users/show', {user: user});
    });
  },
  new: function(req, res) {
    res.view('customer_admin_users/new');
  },
  create: function(req, res) {
    let new_user = new User(req.payload);
    new_user.account_no = req.yar.get('user').account_no;
    let temp_pw = new_user.genTempPW();
    new_user.save(function(err, user) {
      if (err) {
        req.yar.flash('error', better_error(err));
        return res.redirect('/admin/users/new');
      }
      let m = new WelcomeMailer();
      m.send(new_user, temp_pw);
      req.yar.flash('notice', 'You successfully made this user');

      new Notification({
        account_no: new_user.account_no,
        type: 'notice',
        text: `${new_user.name} was created.`,
        link: `#`
      }).save((err) => {console.log(err)});

      return res.redirect(`/admin/users/${user.username}`);
    });
  },
  edit: function(req, res) {
    User.findOne({username: req.params.username}, function(err, user) {
      if (!user) return res(Boom.notFound('User not found'));
      res.view('customer_admin_users/edit', {user: user});
    });
  },
  update: function(req, res) {
    req.payload.account_no = req.yar.get('user').account_no;
    User.findOneAndUpdate({username: req.params.username}, req.payload, function(err, user) {
      if (err) {
        req.yar.flash('error', `Could not find user.`);
        return res.redirect(`/admin/users`)
      }

      req.yar.flash('notice', 'Successfully updated this user.');
      return res.redirect(`/admin/users/${req.payload.username}`);
    });
  },
}
