var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var fs = require("fs");

gulp.task('less', function() {
  return  gulp.src('./uncompiled/less/application.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(gulp.dest('./public/css/'));
});

gulp.task('watch', function() {
  gulp.watch('./uncompiled/less/**/*.less', ['less']);
});

gulp.task('default', ['js', 'less']);
